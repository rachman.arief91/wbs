$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    var dex = 0;
    dataTable = $('.dt-tbl').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + '/screen/list' + param,
            "type": "GET"
        },
        autoWidth: true,
        scrollX: true,
        rowGroup: {
            endRender: function (rows, group) {
                var intVal = function ( i ) {
                    console.log("data to convert : ", isNaN(i));
                    if(isNaN(i)){
                        var withNoU = i.replace(/[-+()\s$,]/g, '');
                        return withNoU;
                    }else{
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    }
                };
                var dataShow = function ( i ) {
                    // if (i < -1) {
                    //     return "(" + ( $.fn.dataTable.render.number('.', ',', 0, '').display((i*-1))) + ")";
                    // }else{
                    //
                    // }
                    return $.fn.dataTable.render.number('.', ',', 0, '').display(i);
                };
                var startTotal = rows.data().pluck('amt_start').reduce(function(a, b){
                    return intVal(a) + intVal(b);
                },0);
                var revTotal = rows.data().pluck('amt_rev').reduce(function(a, b){
                    return intVal(a) + intVal(b);
                },0);
                var trfTotal = rows.data().pluck('amt_trf').reduce(function(a, b){
                    return intVal(a) + intVal(b);
                },0);
                var consumeTotal = rows.data().pluck('amt_consume').reduce(function(a, b){
                    return intVal(a) + intVal(b);
                },0);
                var osTotal = rows.data().pluck('amt_os').reduce(function(a, b){
                    return intVal(a) + intVal(b);
                },0);

                var used = (consumeTotal / (osTotal + consumeTotal)) * 100;
                used = parseFloat(Math.round(used * 100) / 100).toFixed(2);

                return $('<tr/>')
                    .append( '<td class="bg-success" colspan="2">TOTAL '+group+'</td>' )
                    .append( '<td class="right aligned bg-success">'+dataShow(startTotal)+'</td>' )
                    .append( '<td class="right aligned bg-success">'+dataShow(revTotal)+'</td>' )
                    .append( '<td class="right aligned bg-success">'+dataShow(trfTotal)+'</td>' )
                    .append( '<td class="right aligned bg-success">'+dataShow(consumeTotal)+'</td>' )
                    .append( '<td class="right aligned bg-success">'+dataShow(osTotal)+'</td>' )
                    .append( '<td class="right aligned bg-success">'+used+'</td>' );
            },
            startRender: function (rows, group) {
                return $('<tr/>')
                    .append('<td class="bg-slategray" colspan="11">' + group + '</td>');
            },
            dataSrc: 'div_name'
        },
        "columns": [
            {
                data: null, orderable: false, searchable: false, render: function (data, type, row, meta) {
                var pageLength = dataTable.page.info().length;
                var before = dataTable.row(meta.row - 1).data();
                var now = dataTable.row(meta.row).data();
                if (typeof before === 'undefined') {
                    dex = 1;
                }else{
                    if (before.id_div == now.id_div) {
                        dex += 1;
                    }else{
                        dex = 1;
                    }
                }
                return "<div class='center aligned'>"+((dex))+"</div>";
                // return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
            }
            },
            {data: 'budget_desc', name: 'budget_desc', orderable: false, searchable: true},
            {data: 'amt_start', name: 'amt_start', orderable: false, searchable: true, className: "right aligned", render: function (data, type, row, meta) {
                return $.fn.dataTable.render.number('.', ',', 0, '').display(data);
            }},
            {data: 'amt_rev', name: 'amt_rev', orderable: false, searchable: true, className: "right aligned",render: function (data, type, row, meta) {
                return $.fn.dataTable.render.number('.', ',', 0, '').display(data);
            }},
            {data: 'amt_trf', name: 'amt_trf', orderable: false, searchable: true, className: "right aligned",render: function (data, type, row, meta) {
                return $.fn.dataTable.render.number('.', ',', 0, '').display(data);
            }},
            {data: 'amt_consume', name: 'amt_consume', orderable: false, searchable: true, className: "right aligned",render: function (data, type, row, meta) {
                return $.fn.dataTable.render.number('.', ',', 0, '').display(data);
            }},
            {data: 'amt_os', name: 'amt_os', orderable: false, searchable: true, className: "right aligned",render: function (data, type, row, meta) {
                return $.fn.dataTable.render.number('.', ',', 0, '').display(data);
            }},
            {data: 'used', name: 'used', orderable: false, searchable: false, className: "right aligned"}
        ]
    });
});