$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    $.get(base_url + '/screen/column-info' + param, function (resp) {
        var res = JSON.parse(resp);
        dataTable = $('.dt-tbl').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + '/screen/list' + param,
                "type": "GET"
            },
            "columns": res.columns,
            autoWidth: true,
            scrollX: true,
            "columnDefs": [{
                "targets": 0,
                "render": function (data, type, row, meta) {
                    var page = dataTable.page.info();
                    return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
                }
            },
                {
                    "searchable": false,
                    "orderable": true,
                    "visible": false,
                    "targets": 1
                },
                {width: 175, className: "left aligned", targets: [2, 6, 7, 8, 9, 10, 11]},
                {width: 250, targets: [3, 5, 14]},
                {
                    targets: 13,
                    width: 175,
                    searchable: false,
                    className: "center aligned",
                    render: function (data, type, row, meta) {
                        if (data == '1') {
                            return "Pengajuan Baru";
                        } else if (data == '2') {
                            return "Non Active";
                        } else {
                            return "Active";
                        }
                    }
                },
                {
                    targets: 12,
                    searchable: false,
                    render: function (data, type, row, meta) {
                        if (data == '1') {
                            return "<div style='vertical-align: middle;' class='center aligned'><i class='large green checkmark icon'></i>";
                        } else {
                            return "<div class='center aligned'><i class='large red remove icon'></i>";
                        }
                    }
                },

                {
                    targets: res.targets + 1,
                    searchable: false,
                    width: 300,
                    className: "single line center aligned",
                    render: function (data, type, row, meta) {
                        var send = "<div class='form-group ui teal right labeled icon button' onclick='reqInputModal(this,\"screen/send\");'>Send Approval<i class='send icon'></i></div>";
                        var remove = "<div class='form-group ui red right labeled icon button' onclick='showConfirmationDelete(this,\"screen/deleteWD\");'>Delete<i class='trash icon'></i></div>";
                        // return "<div class='form-group ui violet right labeled icon button' onclick='showConfirmation(this,\"screen/deleteWD\");'>Activated<i class='repeat icon'></i></div>";
                        var detail = "<div class='form-group ui orange right labeled icon button' onclick='detailMaster(" + row.id_budget + ");'>Detail<i class='search icon'></i></div>";
                        var edit = "<div class='form-group ui olive right labeled icon button' onclick='modalEdit(this, \"screen/show-edit\");'>Edit<i class='write icon'></i></div>";
                        var revisi = "<div class='form-group ui green right labeled icon button' onclick='modalEdit(this, \"screen/show-edit\");'>Edit<i class='write icon'></i></div>";
                        if (row.wf_seq == '1') {
                            return send + edit;
                        } else if (row.wf_seq == '999') {
                            return send + edit;
                        }else if (row.wf_seq == '500') {
                            return detail;
                        } else {
                            return "-";
                        }
                    }
                }
            ],
            "order": [[1, "desc"]],
            "footerCallback": function (row, data, start, end, display) {
                var response = dataTable.ajax.json();
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$.,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                for (var i = 0; i < response.footer.length; i++) {
                    $(dataTable.column(response.footer[i].index).footer()).html(
                        currencyFormatter(response.footer[i].total)
                    );
                }
                ;

            }
        });
    });
});