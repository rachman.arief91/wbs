$(function(){
    var str = window.location.href,
        param = '';
    if(str.indexOf('/') > -1){
      param = str.slice(str.lastIndexOf('/') + 1);
    }
    dataTable = $('.dt-tbl').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url" : base_url+'/listMaster/'+param,
            "type": "GET"
        },
        "ordering": false,
        autoWidth: true,
        scrollX: true,
        "columns": [
            { data: 'crtdt', name: 'crtdt', visible: false },
            { data: 'div_name', name: 'div_name', orderable: true, searchable: false  },
            { data: 'tx_code', name: 'tx_code', orderable: true, searchable: false},
            { data: 'reversal_status', name: 'reversal_status', orderable: true, searchable: false},
            { data: 'ket_tx', name: 'ket_tx', orderable: true, searchable: false },
            { data: null, orderable: false, searchable: false, className: "right aligned", render: function (data, type, row, meta) {
                if(row.tx_type == 1){
                    return '-';
                }else{
                    return row.amt_tx;
                }

            }},
            { data: null, orderable: false, searchable: false , className: "right aligned", render: function (data, type, row, meta) {
                if(row.tx_type == 1){
                    return row.amt_tx;
                }else{
                    return '-';
                }
            }},
            { data: 'amt_os', name: 'amt_os', orderable: true, searchable: false , className: "right aligned" },
            { data: null, orderable: false, className: "center aligned",searchable: false ,  render : function ( data, type, row, meta ) {
                return "<div class='form-group ui orange right labeled icon button' onclick='detailTx(" + row.id_tx + ");'>Detail<i class='search icon'></i></div>";
            } }
        ]
    });
  });