$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    dataTable = $('.dt-tbl').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + '/screen/list' + param,
            "type": "GET"
        },
        autoWidth: true,
        scrollX: true,
        "columns": [
            {
                orderable: false, searchable: false, render: function (data, type, row, meta) {
                var page = dataTable.page.info();
                return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
            }
            },
            {data: 'name', name: 'name', orderable: true, searchable: true},
            {data: 'div_name', name: 'reff_div.div_name', orderable: true, searchable: true},
            {data: 'nama_jabatan', name: 'reff_jabatan.nama_jabatan', orderable: true, searchable: true},
            {data: 'role_name', name: 'role.role_name', orderable: true, searchable: true},
            {
                data: null, orderable: false, searchable: false, className: "single line center aligned",
                render: function (data, type, row, meta) {
                    var del = "<div class='form-group ui red right labeled icon button' onclick='showConfirmationDelete(this,\"deleteUser\");'>Delete<i class='trash icon'></i></div>";
                    var edit = "<div class='form-group ui olive right labeled icon button' onclick='editDT(this, \"editUser\");'>Edit<i class='write icon'></i></div>";
                    if (row.role_type == '1'){
                        return edit;
                    }
                    return edit+del;
                }

            }
        ]
    });
});