$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    $.get(base_url + '/screen/column-info' + param, function (resp) {
        var res = JSON.parse(resp);
        dataTable = $('.dt-tbl').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + '/screen/list' + param,
                "type": "GET"
            },
            "columns": res.columns,
            autoWidth: true,
            scrollX: true,
            "columnDefs": [{
                "targets": 0,
                "render": function (data, type, row, meta) {
                    var page = dataTable.page.info();
                    return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
                }
            },
                {
                    "searchable": false,
                    "orderable": true,
                    "visible": false,
                    "targets": 1
                },
                {
                    width: 300,
                    targets: res.targets + 1,
                    searchable: false,
                    className: "center aligned",
                    render: function (data, type, row, meta) {
                        var dateParts = row.crtdt.split(" ")[0].split("-");
                        var dateObject = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
                        var today = new Date();
                        var send = "<div class='form-group ui teal right labeled icon button' onclick='reqInputModal(this,\"screen/send\");'>Send Approval<i class='send icon'></i></div>";
                        var remove = "<div class='form-group ui red right labeled icon button' onclick='showConfirmationDelete(this,\"screen/deleteWD\");'>Delete<i class='trash icon'></i></div>";
                        var detail = "<div class='form-group ui orange right labeled icon button' onclick='trxDetail(" + row.id_tx + ");'>Detail<i class='search icon'></i></div>";
                        var editOriginal = "<div class='form-group ui olive right labeled icon button' onclick='editDT(this, \"trxEdit\");'>Edit<i class='write icon'></i></div>";
                        var editReversal = "<div class='form-group ui olive right labeled icon button' onclick='editDT(this, \"trxEdit\");'>Edit<i class='write icon'></i></div>";
                        if (row.wf_seq == '1') {
                            return send + editOriginal;
                        } else if (row.wf_seq == '999') {
                            if (today.getDay() == dateObject.getDay()){
                                return send + editOriginal;
                            }else{
                                return '-';
                            }
                        } else if (row.wf_seq == '11' || row.wf_seq == '998') {
                            return send;
                        }  else if (row.wf_seq == '998') {
                            if (today.getDay() == dateObject.getDay()){
                                return send;
                            }else{
                                return '-';
                            }
                        }else if (row.wf_seq == '500') {
                            return detail;
                        } else {
                            return "-";
                        }
                    }
                }
            ],
            "order": [[1, "desc"]],
            "footerCallback": function (row, data, start, end, display) {
                var response = dataTable.ajax.json();
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$.,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                for (var i = 0; i < response.footer.length; i++) {
                    $(dataTable.column(response.footer[i].index).footer()).html(
                        currencyFormatter(response.footer[i].total)
                    );
                }
                ;

            }
        });
    });
});