$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    var dex = 0;
    dataTable = $('.dt-tbl').DataTable({
        "processing": true,
        "serverSide": true,
        "ordering": false,
        "ajax": {
            "url": base_url + '/screen/list' + param,
            "type": "POST",
            "data":function ( d ) {
                d.startDate = $("#start_date").val();
                d.endDate = $("#end_date").val();
            }
        },
        autoWidth: true,
        scrollX: true,
        rowGroup: {
            endRender: null,
            startRender: function (rows, group) {
                var res = group.split("###");
                var name = '<div class="ui green label center">'+res[0]+'</div>';
                var uname = '<div class="ui yellow label center">'+res[1]+'</div>';
                return $('<tr/>')
                    .append('<td class="bg-slategray" colspan="11">' + name + uname + '</td>');
            },
            dataSrc: 'name'
        },
        "columns": [
            {data: 'budget_code', name: 'budget_code', orderable: false, searchable: true},
            // {data: 'name', name: 'name', orderable: false, searchable: true},
            {data: 'tx_code', name: 'tx_code', orderable: false, searchable: true},
            {data: 'account_no', name: 'account_no', orderable: false, searchable: true},
            {data: 'account_name', name: 'account_name', orderable: false, searchable: true},
            { data: null, orderable: false, searchable: false, className: "right aligned", render: function (data, type, row, meta) {
                if(row.id_tx_type == 1){
                    return '-';
                }else{
                    return row.amt_tx;
                }

            }},
            { data: null, orderable: false, searchable: false , className: "right aligned", render: function (data, type, row, meta) {
                if(row.id_tx_type == 1){
                    return row.amt_tx;
                }else{
                    return '-';
                }
            }},
            // {data: 'amt_tx', name: 'amt_tx', orderable: false, searchable: true, className: "right aligned"},
            {data: 'ket_tx', name: 'ket_tx', orderable: false, searchable: true},
            {data: 'crtdt', name: 'crtdt', orderable: false, searchable: true}
        ]
    });
});