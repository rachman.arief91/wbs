$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }

    $.get(base_url + '/screen/column-info' + param, function (resp) {
        var res = JSON.parse(resp);
        dataTable = $('.dt-tbl').DataTable({
            responsive: true,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + '/screen/list' + param,
                "type": "GET"
            },
            "columns": res.columns,
            "autoWidth": true,
            scrollX: true,
            deferRender: true,
            'initComplete': function(settings){
                var api = this.api();
                api.cells(
                    api.rows(function(idx, data, node){
                        return (data.wf_seq != '1') ? true : false;
                    }).indexes(),
                    0
                ).checkboxes.disable();
            },
            "columnDefs": [
                {
                    'targets': 0,
                    width: 100,
                    'searchable': false,
                    'orderable': false,
                    'checkboxes': {
                        'selectRow': true
                    }
                },
                {
                    "searchable": false,
                    "orderable": false,
                    "data": null,
                    "targets": 1,
                    "render": function (data, type, row, meta) {
                        var page = dataTable.page.info();
                        return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
                    }
                },
                {
                    "searchable": false,
                    "orderable": true,
                    "visible": false,
                    "targets": 2
                },
                {width: 150, targets: [3, 4, 5, 6, 7, 8,9, 10, 11]},
                {
                    targets: res.targets + 1,
                    searchable: false,
                    width: 300,
                    className: "single line center aligned",
                    render: function (data, type, row, meta) {
                       if (row.wf_seq == '600'){
                           return "<div class='form-group ui olive right labeled icon button' onclick='editDT(this, \"settlement\");'>Pelunasan<i class='refresh icon'></i></div>";
                       }
                       return '-';
                    }
                }
            ],
            'select': {
                'style': 'multi',
                'selector': 'tr:not(.no-select)'
            },
            "order": [[2, "desc"]],
            "footerCallback": function (row, data, start, end, display) {
                var response = dataTable.ajax.json();
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$.,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                for (var i = 0; i < response.footer.length; i++) {
                    $(dataTable.column(response.footer[i].index).footer()).html(
                        currencyFormatter(response.footer[i].total)
                    );
                }
            }
        });
    });
});

function checkAllInput(elm,wfSeq, sendType, url){
    var data = dataTable
        .rows()
        .data();
    var count = 0;
    $.each(data, function(index, item){
        if (item.wf_seq == 1){
            count = count + 1;
        }
    });
    if(count > 0){
        showParentModal(elm,wfSeq,sendType, url);
    }else{
        toastr.error('Tidak Ada Data yang bisa dikirim untuk approval');
    }

}