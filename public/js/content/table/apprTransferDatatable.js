$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    dataTable = $('.dt-tbl').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + '/screen/list' + param,
            "type": "GET"
        },
        autoWidth: true,
        scrollX: true,
        deferRender: true,
        rowId: 'id_transfer',
        "columns": [
            {
                data: 'id_transfer', orderable: false, searchable: false, 'checkboxes': {
                'selectRow': true
            }
            },
            {
                searchable: false, render: function (data, type, row, meta) {
                var page = dataTable.page.info();
                return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
            }
            },
            {data: 'name', name: 'name', orderable: true, searchable: false,className: "left aligned"},
            {data: 'crtdt', name: 'crtdt', orderable: true, searchable: false},
            {data: 'from_div', name: 'from_div', orderable: true, searchable: false},
            {data: 'from_desc', name: 'from_desc', orderable: true, searchable: false},
            {data: 'from_amt', name: 'from_amt', orderable: true, searchable: false, className: "right aligned"},
            {data: 'to_div', name: 'to_div', orderable: true, searchable: false},
            {data: 'to_desc', name: 'to_desc', orderable: true, searchable: false},
            {data: 'to_amt', name: 'to_amt', orderable: true, searchable: false, className: "right aligned"},
            {data: 'nominal', name: 'nominal', orderable: true, searchable: false, className: "right aligned"},
            {data: 'nama_wf_status', name: 'nama_wf_status', orderable: true, searchable: false},
            {
                data: null, orderable: false, searchable: false, className: "single line center aligned",
                render: function (data, type, row, meta) {
                    return "<div class='form-group ui orange right labeled icon button' onclick='modalDetail(this, \"screen/show-detail\");'>Detail<i class='search icon'></i></div>";
                }
            }
        ],
        "order": [[3, "desc"]],
        'select': {
            'style': 'multi'
        }
    });
});