$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    dataTable = $('.dt-tbl').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + '/screen/list' + param,
            "type": "GET"
        },
        autoWidth: true,
        scrollX: true,
        "columns": [
            {
                searchable: false, render: function (data, type, row, meta) {
                var page = dataTable.page.info();
                return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
            }
            },
            {data: 'crtdt', name: 'crtdt', orderable: true, searchable: false},
            {data: 'from_div', name: 'from_div', orderable: true, searchable: false},
            {data: 'from_desc', name: 'from_desc', orderable: true, searchable: false},
            {data: 'from_amt', name: 'from_amt', orderable: true, searchable: false, className: "right aligned"},
            {data: 'to_div', name: 'to_div', orderable: true, searchable: false},
            {data: 'to_desc', name: 'to_desc', orderable: true, searchable: false},
            {data: 'to_amt', name: 'to_amt', orderable: true, searchable: false, className: "right aligned"},
            {data: 'nominal', name: 'nominal', orderable: true, searchable: false, className: "right aligned"},
            {data: 'nama_wf_status', name: 'nama_wf_status', orderable: true, searchable: false},
            {
                data: null, orderable: false, searchable: false, className: "single line center aligned",
                render: function (data, type, row, meta) {
                    var send = "<div class='form-group ui teal right labeled icon button' onclick='reqInputModal(this,\"screen/send\");'>Send Approval<i class='send icon'></i></div>";
                    var edit = "<div class='form-group ui olive right labeled icon button' onclick='modalEdit(this, \"screen/show-edit\");'>Edit<i class='write icon'></i></div>";
                    if(row.wf_seq == '1' ){
                        return send+edit;
                    }
                    return "-";
                }

            }
        ],
        "order": [[1, "desc"]]
    });
});