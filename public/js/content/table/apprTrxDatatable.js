$(function(){
  var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    
    $.get(base_url+'/screen/column-info'+param, function(resp){
      var res = JSON.parse(resp);
      dataTable = $('.dt-tbl').DataTable({
        responsive: true,
        "processing": true,
        "serverSide": true,
        "ajax": {
          "url" : base_url+'/screen/list'+param,
          "type": "GET"
        },
        "columns": res.columns,
        "autoWidth":true,
        scrollX:        true,
        deferRender:    true,
        "columnDefs": [
          {
             'targets': 0,
             'searchable': false,
             'orderable': false,
             'checkboxes': {
                'selectRow': true
             }
          },
          {
            "searchable": false,
            "orderable": false,
            "data": null,
            "targets": 1,
            "render": function( data, type, row, meta ){
                var page = dataTable.page.info();
                return "<div class='center aligned'>"+((page.length*page.page)+(meta.row+1))+"</div>";
            }
          },
          {
            "searchable": false,
            "orderable": true,
            "visible":false,
            "targets": 2
          },{
                targets: res.targets + 1,
                searchable: false,
                className: "single line center aligned",
                render: function (data, type, row, meta) {
                    var detail = "<div class='form-group ui orange right labeled icon button' onclick='trxDetail(" + row.id_tx + ");'>Detail<i class='search icon'></i></div>";
                    return detail;
                }
            }
        ],
        'select': {
          'style': 'multi',
        },
        "order": [[ 2, "desc" ]],
        "footerCallback": function ( row, data, start, end, display ) {
          var response = dataTable.ajax.json();
          console.log("response footer", response);
          var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$.,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
          for (var i = 0; i < response.footer.length; i++) {
            $( dataTable.column( response.footer[i].index).footer() ).html(
                currencyFormatter(response.footer[i].total)
              );
            };
        }
      });
    });
})