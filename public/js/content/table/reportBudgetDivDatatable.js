$(function () {
    var str = window.location.href,
        param = '';
    if (str.indexOf('?') > -1) {
        param = str.slice(str.indexOf('?'));
    }
    dataTable = $('.dt-tbl').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + '/screen/list' + param,
            "type": "GET"
        },
        autoWidth: true,
        scrollX: true,
        "columns": [
            {
                orderable: false, searchable: false, render: function (data, type, row, meta) {
                var page = dataTable.page.info();
                return "<div class='center aligned'>" + ((page.length * page.page) + (meta.row + 1)) + "</div>";
            }
            },
            {data: 'div_name', name: 'reff_div.div_name', orderable: true, searchable: true},
            {data: 'amt_start', name: 'amt_start', orderable: true, searchable: true,  className: "right aligned"},
            {data: 'amt_rev', name: 'amt_rev', orderable: true, searchable: true, className: "right aligned"},
            {data: 'amt_trf', name: 'amt_trf', orderable: true, searchable: true, className: "right aligned"},
            {data: 'amt_consume', name: 'amt_consume', orderable: true, searchable: true, className: "right aligned"},
            {data: 'amt_os', name: 'amt_os', orderable: true, searchable: true, className: "right aligned"},
            {data: 'used', name: 'used', orderable: true, searchable: false, className: "right aligned"}
        ],
        "order": [[1, "desc"]]
    });
});