var dataTable, rows_selected;
 $.extend(window.Parsley.options, {
   excluded: 'input[type=button], input[type=submit], input[type=reset], .search, .ignore, .input-mini',
  // inputs: 'input, textarea, select, input[type=hidden], :hidden',
   //trigger:'input change blur',
   triggerAfterFailure: 'input change',
   errorsContainer: function (element) {
   if (element.$element.attr('type') === 'radio' || element.$element.attr('type') === 'checkbox'){
    return element.$element.closest('.segment').parent();
    }else{
    //console.log('errorsContianer:',element.$element,element.$element.closest('.field'));
    return element.$element.closest('.field'); 
    }
    },
  classHandler: function ( element ) {
    if (element.$element.attr('type') === 'radio' || element.$element.attr('type') === 'checkbox'){
    return element.$element.closest('.segment').parent();
    }else{
     //console.log('classHandler:',element.$element,element.$element.closest('.field'));
    return element.$element.closest('.field'); 
    }
    
    },
   errorTemplate: '<div class="ui basic yellow pointing prompt label transition scale in"></div>',
   errorsWrapper: '<div></div>',
   errorClass: 'error',
   successClass: 'success'
});
Parsley.addValidator('fixLength', {
    validateString: function (value, requirement) {
        return value.length == requirement;
    },
    messages: {
        en: "must be %s digit",
        in: "harus %s digit"
    }
});
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};
$(function(){
  //!!!!!!!!!!!!!
  // initializing
  //!!!!!!!!!!!!!
  $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
  $(document).ready(function(){
    console.log('ready');
    MINOVATE.documentOnReady.init();
    $('#form').trigger('chosen:updated');
  });
  $(window).on( 'resize', MINOVATE.documentOnResize.init );
  
})

window.addEventListener('popstate', function(event) {
    console.log('popstate');
    $('.modal').modal('hide');
    $('.ui.dimmable #loader').dimmer('destroy');
    $('body .modals').remove();
    if (event.state == null) {
      window.location.href = base_url;
    }else{
      if(event.state.hasOwnProperty('url')){
        console.log('popstate url');
        showPage(event.state.url, true);
      }
    }
}, false)