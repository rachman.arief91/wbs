var alertRedInput = "#f3b616";
var defaultInput = "rgba(158, 44, 33, 1)";

function userNameValidation(usernameInput) {
    var username = document.getElementById("username");
    var error = document.getElementById("error-msg");
    error.innerHTML = ""
    username.setCustomValidity("");
    username.style.borderColor = defaultInput;
}

function passwordValidation(passwordInput) {
    var password = document.getElementById("password");
    var error = document.getElementById("error-msg");
    error.innerHTML = ""
    password.setCustomValidity("");
    password.style.borderColor = defaultInput;
}
$('input').keypress(function(e){
    var code = e.keyCode || e.which;
    if( code === 13 ) {
        e.preventDefault();
        $( "#login-btn" ).click();
        return false;
    };
});
$('#login-btn').on('click', function(event){
    var error = document.getElementById("error-msg");
    error.innerHTML = ""
    event.preventDefault();
    var form = $('#formLogin');
    form.parsley().validate();

    if (!form.parsley().isValid()) {
      return false;
    }else{
        console.log('valid');
        var loader = $('.ui.dimmable #main_loader');
        loader.dimmer({closable: false}).dimmer('show');
        formData = new FormData($('form')[0]);
        formData.set("password", $.md5(password.value))
        $.ajax({
              processData: false,
              contentType: false,
              type: 'POST',
              url: 'login',
              data: formData,
              success: function (response) {
                loader.dimmer({closable: false}).dimmer('hide');
                if(response.rc == 0){
                    document.getElementById('error-msg').innerHTML = response.rm
                    window.location.href = 'home';
                }else{
                    document.getElementById('error-msg').innerHTML = response.rm
                    document.getElementById("password").value = ""
                }
              },
              error: function(data){
                loader.dimmer({closable: false}).dimmer('hide');
                console.log(data);
              }
            });
    }
});
