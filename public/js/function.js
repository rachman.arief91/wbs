function currencyFormatter(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function ajaxMenu(e, elm){
  e.preventDefault();
  var url = $(elm).attr('href').replace('#', '');
  if(url){
    showPage(url);
  }
}
function showPage(url, skip){
    var state = {name: "name", page: 'History', url:url};
    console.log("sub url : " + base_url+'/'+url);
    subPage(url);
    if(typeof skip === 'undefined'){
      window.history.pushState(state, "History",base_url+'/'+url);
    }else{
      window.history.replaceState(state, "History",base_url+'/'+url);
    }
}
function showLoading(){
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
}
function hideLoading(destroy){
  var loader = $('.ui.dimmable #main_loader');
  loader.dimmer({closable: false}).dimmer('hide');
  if (destroy) {
      $('.modal').modal('hide');
      $('.ui.dimmable #loader').dimmer('destroy');
      $('body .modals').remove();
  }
}
function subPage(url){
  showLoading();
  $.get(base_url+'/'+url, function(response, status, xhr){
      hideLoading(true);
      var res = $.parseJSON(response);
      $('#data_content').html(res.content);
      $('#data_modal').html(res.modals);
      $('#data_script').html(res.scripts);
      MINOVATE.extra.init();
  }).fail(function(response) {
      hideLoading(false);
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
  });
}
function loadMenu(url, skip){
  console.log("url : " + base_url+'/'+url);
  var state = {name: "name", page: 'History', url:url};
  if(typeof skip === 'undefined'){
    window.history.pushState(state, "History",base_url+'/'+url);
  }
  var loader = $('.ui.dimmable #main_loader');
  loader.dimmer({closable: false}).dimmer('show');
  $.get(base_url+'/'+url, function(response, status, xhr){
      var res = $.parseJSON(response);
      loader.dimmer({closable: false}).dimmer('hide');
      $('.modal').modal('hide');
      $('.ui.dimmable #loader').dimmer('destroy');
      $('body .modals').remove();
      $('#data_content').html(res.content);
      $('#data_modal').html(res.modals);
      $('#data_script').html(res.scripts);
      MINOVATE.extra.init();
  }).fail(function(response) {
      loader.dimmer({closable: false}).dimmer('hide');
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
  });
}
function detailMaster(url){
    url = 'detailMaster/'+url;
    console.log("url : " + base_url+ "/"+url);
    var state = {name: "name", page: 'History', url:url};
    window.history.pushState(state, "History",base_url+ "/"+url);
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    $.get(base_url + "/" + url, function(response, status, xhr){
        var res = $.parseJSON(response);
        loader.dimmer({closable: false}).dimmer('hide');
        $('.modal').modal('hide');
        $('.ui.dimmable #loader').dimmer('destroy');
        $('body .modals').remove();
        $('#data_content').html(res.content);
        $('#data_modal').html(res.modals);
        $('#data_script').html(res.scripts);
        MINOVATE.extra.init();
    }).fail(function(response) {
        loader.dimmer({closable: false}).dimmer('hide');
        console.log(response);
        if(response.status == 401){
            window.location.href = base_url;
        } else {
            toastr.error('Terjadi Kesalahan');
        }
    });
}
function trxDetail(url){
    url = 'trxDetail/'+url;
    console.log("url : " + base_url+ "/"+url);
    var state = {name: "name", page: 'History', url:url};
    window.history.pushState(state, "History",base_url+ "/"+url);
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    $.get(base_url+ "/"+url, function(response, status, xhr){
        var res = $.parseJSON(response);
        loader.dimmer({closable: false}).dimmer('hide');
        $('.modal').modal('hide');
        $('.ui.dimmable #loader').dimmer('destroy');
        $('body .modals').remove();
        $('#data_content').html(res.content);
        $('#data_modal').html(res.modals);
        $('#data_script').html(res.scripts);
        MINOVATE.extra.init();
    }).fail(function(response) {
        loader.dimmer({closable: false}).dimmer('hide');
        console.log(response);
        if(response.status == 401){
            window.location.href = base_url;
        } else {
            toastr.error('Terjadi Kesalahan');
        }
    });
}
function detailTx(url){
    url = 'detailTx/'+url;
    console.log("url : " + base_url+ "/"+url);
    var state = {name: "name", page: 'History', url:url};
    window.history.pushState(state, "History",base_url+ "/"+url);
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    $.get(base_url+ "/"+url, function(response, status, xhr){
        var res = $.parseJSON(response);
        loader.dimmer({closable: false}).dimmer('hide');
        $('.modal').modal('hide');
        $('.ui.dimmable #loader').dimmer('destroy');
        $('body .modals').remove();
        $('#data_content').html(res.content);
        $('#data_modal').html(res.modals);
        $('#data_script').html(res.scripts);
        MINOVATE.extra.init();
    }).fail(function(response) {
        loader.dimmer({closable: false}).dimmer('hide');
        console.log(response);
        if(response.status == 401){
            window.location.href = base_url;
        } else {
            toastr.error('Terjadi Kesalahan');
        }
    });
}
function addForm(url){
    var str = window.location.href,
        param = '';
    if(str.indexOf('?') > -1){
        param = str.slice(str.indexOf('?'));
    }
    loadMenu(url+param);
}
function editDT(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
      param = '';

  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }

  loadMenu(url+ "/" +id+param);
}
function nestedAdd(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
      param = '';

  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }

  loadMenu(url+"&id="+id);
}
function modalAdd(url){
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    var modal = $('#modalEdit').modal({closable  : false,autofocus: false,});
    $.get(base_url+'/'+url, function(response, status, xhr){
      loader.dimmer({closable: false}).dimmer('hide');
      var res = $.parseJSON(response);
      console.log(res.content);
      $('#modalEditTitle').text(res.title);
      $('#modalEditContent').html(res.content); 
      $('#modalEditAction').html(res.footer);
      MINOVATE.extra.init();
      modal.modal("show");
    }).fail(function(response) {
       loader.dimmer({closable: false}).dimmer('hide');
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
    });
}
function modalEdit(elm, url){
  var str = window.location.href,
      id = $(elm).closest('tr').attr('id')
      param = '';
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    var modal = $('#modalEdit').modal({closable  : false,autofocus: false,});
    $.get(url+ "/" +id+param, function(response, status, xhr){
      loader.dimmer({closable: false}).dimmer('hide');
      var res = $.parseJSON(response);
      console.log(res.content);
      $('#modalEditTitle').text(res.title);
      $('#modalEditContent').html(res.content); 
      $('#modalEditAction').html(res.footer);
      MINOVATE.extra.init();
      modal.modal("show");
    }).fail(function(response) {
       loader.dimmer({closable: false}).dimmer('hide');
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
    });
}
function modalDetail(elm, url){
    var str = window.location.href,
        id = $(elm).closest('tr').attr('id')
    param = '';
    if(str.indexOf('?') > -1){
        param = str.slice(str.indexOf('?'));
    }
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    var modal = $('#modalDetail').modal({closable  : false,autofocus: false,});
    $.get(url+ "/" +id+param, function(response, status, xhr){
        loader.dimmer({closable: false}).dimmer('hide');
        var res = $.parseJSON(response);
        console.log(res.content);
        $('#modalDetailTitle').text(res.title);
        $('#modalDetailContent').html(res.content);
        MINOVATE.extra.init();
        modal.modal("show");
    }).fail(function(response) {
        loader.dimmer({closable: false}).dimmer('hide');
        console.log(response);
        if(response.status == 401){
            window.location.href = base_url;
        } else {
            toastr.error('Terjadi Kesalahan');
        }
    });
}
function showConfirmationDelete(elm, idRoute){ 
  var rowId = $(elm).closest('tr').attr('id'),
    oncClick = typeof idRoute === 'undefined'?'del("' + rowId + '");':'del("' + rowId + '","'+idRoute+'");';
  $('#confirmDeleteBtn').attr('onclick', oncClick);
  $('#confirmDeleteModal').modal('show')
}
function showConfirmation(elm, idRoute){
  var rowId = $(elm).closest('tr').attr('id'),
    oncClick = typeof idRoute === 'undefined'?'del("' + rowId + '");':'del("' + rowId + '","'+idRoute+'");';

  $('#submitBtn').attr('onclick', oncClick);
  $('#confirmAlertModal').modal('show');
}
function showConfirmationAppr(elm, idRoute){
  var oncClick = 'get("'+idRoute+'");';
  $('#submitBtn').attr('onclick', oncClick);
  $('#confirmAlertModal').modal('show');
}
function showConfirmationGen(elm, oncClick){
  $('#submitBtn').attr('onclick', oncClick);
  $('#confirmAlertModal').modal('show');
}
function reqInputKetModal(elm, route){
    var rowId = $(elm).closest('tr').attr('id');
    var url = base_url+ "/" + route + "/" + rowId;
    if(typeof rowId === 'undefined'){
        url = base_url+ "/" + route;
    }
    var str = window.location.href,
        param = '',
        form = $('#formInputKet');
    if(str.indexOf('?') > -1){
        param = str.slice(str.indexOf('?'));
    }
    url = url + param;
    console.log("url : " , url);
    form[0].reset();
    $("#submitKetInput").off('click').on('click', function() {
        form.parsley().validate();
        var formData = new FormData(form[0]);
        if (!form.parsley().isValid()) {
            return false;
        }else{
            for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }
            sendReqInputAll(formData, url);
        }
    });
    $('#inputMsgKetModal').modal('show');
}
function reqInputModal(elm, route){
    var rowId = $(elm).closest('tr').attr('id');
    var url = base_url+ "/" + route + "/" + rowId;
    if(typeof rowId === 'undefined'){
        url = base_url+ "/" + route;
    }
    var str = window.location.href,
        param = '',
        form = $('#formInput');
    if(str.indexOf('?') > -1){
        param = str.slice(str.indexOf('?'));
    }
    url = url + param;
    console.log("url : " , url);
    form[0].reset();
    $("#submitInput").off('click').on('click', function() {
        form.parsley().validate();
        var formData = new FormData(form[0]);
        if (!form.parsley().isValid()) {
            return false;
        }else{
            for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]);
            }
            sendReqInputAll(formData, url);
        }
    });
    $('#inputMsgModal').modal('show');
}
function sendReqInputAll(formData, url) {
    $('.modal').each(function(){
        $(this).modal('hide');
    });
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
            loader.dimmer({closable: false}).dimmer('hide');
            if(res.rc == 0){
                toastr.success('Sukses');
                dataTable.ajax.reload();
            } else {
                toastr.error(res.rm);
            }
        },
        error: function(data){
            loader.dimmer({closable: false}).dimmer('hide');
            toastr.error('Terjadi Kesalahan');
        }
    });
}
function showParentModal(elm, wfSeq, sendType, url) {
    var str = window.location.href,
        param = '',
        form = $('#formInput'),
        rowSelected = null,
        selected = [];
    if(str.indexOf('?') > -1){
        param = str.slice(str.indexOf('?'));
    }
    url = url + param;
    if (sendType == 1){
        rowSelected = dataTable.column(0).checkboxes.selected();
    }else {
        rowSelected = dataTable.column(0).data();
    }
    $.each(rowSelected, function(index, rowId){
        selected.push(rowId);
    });
    console.log("rowSelected : " , selected);
    console.log("url : " , url);
    form[0].reset();
    if(selected.length == 0){
        toastr.error('Silahkan Pilih Minimal Satu');
    }else{

        getAtasanReff(form,selected, $('#next_seq'), 'reff/6/' + wfSeq, url)

    }
}
function getAtasanReff(form,selected, elm, url, appUrl) {
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    console.log("get reff");
    $.get(base_url + '/' + url, function (response, status, xhr) {
        loader.dimmer({closable: false}).dimmer('hide');
        console.log(response);
        elm.dropdown('clear');
        elm.empty();
        elm.append('<option value="">Pilih</option>');
        elm.dropdown("refresh");
        if (response.length > 0) {
            for (var i = 0; i < response.length; i++) {
                var def = '<option value=' + response[i].value + '>' + response[i].print + '</option>';
                elm.append(def);
            }
        }
        $("#submitInput").off('click').on('click', function() {
            form.parsley().validate();
            var formData = new FormData(form[0]);
            if (!form.parsley().isValid()) {
                return false;
            }else{
                formData.append('selected', selected);
                for (var pair of formData.entries()) {
                    console.log(pair[0]+ ', ' + pair[1]);
                }
                sendInputAll(formData, appUrl);
            }
        });
        var modal = $('#inputParentModal').modal({autofocus: false});
        modal.modal('show');


    }).fail(function (response) {
        loader.dimmer({closable: false}).dimmer('hide');
        console.log(response);
        if (response.status == 401) {
            window.location.href = base_url;
        } else {
            toastr.error('Terjadi Kesalahan');
        }
    });
}


function showInputModal(elm, sendType, url, appr){
    var str = window.location.href,
        param = '',
        form = $('#formInput'),
        rowSelected = null,
        selected = [];
    if(str.indexOf('?') > -1){
        param = str.slice(str.indexOf('?'));
    }
    url = url + param;
    if (sendType == 1){
        rowSelected = dataTable.column(0).checkboxes.selected();
    }else {
        rowSelected = dataTable.column(0).data();
    }
    $.each(rowSelected, function(index, rowId){
        selected.push(rowId);
    });
    console.log("rowSelected : " , selected);
    console.log("url : " , url);
    form[0].reset();
    if(selected.length == 0){
        toastr.error('Silahkan Pilih Minimal Satu');
    }else{
        $("#submitInput").off('click').on('click', function() {
            form.parsley().validate();
            var formData = new FormData(form[0]);
            if (!form.parsley().isValid()) {
                return false;
            }else{
                formData.append('selected', selected);
                for (var pair of formData.entries()) {
                    console.log(pair[0]+ ', ' + pair[1]);
                }
                sendInputAll(formData, url);
            }
        });
        if (appr == "1"){
            $('#msgText').val("Setuju Untuk Diproses");
        }
        $('#inputMsgModal').modal('show');
    }
}
function sendInputAll(formData, url) {
    $('.modal').each(function(){
        $(this).modal('hide');
    });
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
            loader.dimmer({closable: false}).dimmer('hide');
            if(res.rc == 0){
                toastr.success('Sukses');
                loadMenu(res.url);
            } else {
                toastr.error(res.rm);
            }
        },
        error: function(data){
            loader.dimmer({closable: false}).dimmer('hide');
            toastr.error('Terjadi Kesalahan');
        }
    });
}
function saveRevisi(id){
    if ($(".currency").length>0) {
        $(".currency").each(function(){
            $(this).val($(this).autoNumeric('get'));
        });
    }
    if ($(".percent").length>0) {
        $(".percent").each(function(){
            $(this).val($(this).autoNumeric('get'));
        });
    }
    var str = window.location.href,
        param = '',
        url = base_url+'/screen/saveRevisi',
        form = $('#formEdit'),
        formData = new FormData(form[0]);
    if(str.indexOf('?') > -1){
        param = str.slice(str.indexOf('?'));
    }
    if(id == undefined){
        url += param;
    } else {
        url += "/" + id + param;
    }
    // for (var pair of formData.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]);
    // }
    form.parsley().validate();
    if (!form.parsley().isValid()) {
        return false;
    }else{
        if(formData.get('password') != null){
            formData.set("password", $.md5(formData.get('password')));
        }
        var loader = $('.ui.dimmable #loader');
        loader.dimmer({closable: false}).dimmer('show');
        $("#btnSubmit").addClass('disabled');
        $.ajax({
            processData: false,
            contentType: false,
            type: 'POST',
            url: url,
            data: formData,
            success: function (res) {
                loader.dimmer('hide');
                $("#btnSubmit").removeClass('disabled');
                if(res.rc == 0){
                    toastr.success('Sukses');
                    form[0].reset();
                    $('#modalEdit').modal("hide");
                    dataTable.ajax.reload();
                } else {
                    toastr.error(res.rm);
                }
            },
            error: function(data){
                loader.dimmer('hide');
                $("#btnSubmit").removeClass('disabled');
                toastr.error('Terjadi Kesalahan');
            }
        });
    }
}

function save(id){
  if ($(".currency").length>0) {
        $(".currency").each(function(){
          $(this).val($(this).autoNumeric('get'));
        });
    }
  if ($(".percent").length>0) {
      $(".percent").each(function(){
        $(this).val($(this).autoNumeric('get'));
      });
    }
    var str = window.location.href,
        param = '',
        url = base_url+'/screen/save',
        form = $('#formEdit'),
        formData = new FormData(form[0]);
    if(str.indexOf('?') > -1){
      param = str.slice(str.indexOf('?'));
    }
    if(id == undefined){
        url += param;
      } else {
        url += "/" + id + param;
      }

    for (var pair of formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }
    form.parsley().validate();
    if (!form.parsley().isValid()) {
      return false;
    }else{
      if(formData.get('password') != null){
        formData.set("password", $.md5(formData.get('password')));
      }
      var loader = $('.ui.dimmable #loader');
      loader.dimmer({closable: false}).dimmer('show');
      $("#btnSubmit").addClass('disabled');
      $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
          loader.dimmer('hide');
          $("#btnSubmit").removeClass('disabled');
          if(res.rc == 0){
            toastr.success('Sukses');
            form[0].reset();
            $('#modalEdit').modal("hide");
            // logic for add or edit form closable

            // if(id == undefined){
            //     form[0].reset();
            // } else {
            //
            // }
            dataTable.ajax.reload();
          } else {
            toastr.error(res.rm);
          }
        },
        error: function(data){
          loader.dimmer('hide');
          $("#btnSubmit").removeClass('disabled');
          toastr.error('Terjadi Kesalahan');
        }
      });
    }
}
function del(id, idRoute){
  var str = window.location.href,
      route = typeof idRoute === 'undefined'?$("#routeDelete").val():idRoute,
      url = base_url+ "/" + route + "/" + id,
      param = '';
  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }
  url = url + param;
  $('.modal').each(function(){
        $(this).modal('hide');
  });
  var loader = $('.ui.dimmable #main_loader');
  loader.dimmer({closable: false}).dimmer('show');
  $.get(url, function(response, status, xhr){
      loader.dimmer({closable: false}).dimmer('hide');
      if(response.rc == 0){
        toastr.success('Sukses');
        dataTable.ajax.reload();
      } else {
        toastr.error(response.rm);
      }
    }).fail(function(response) {
      loader.dimmer({closable: false}).dimmer('hide');
      toastr.error('Terjadi Kesalahan');
    });
}

function sendSelected(url){
  var formData = new FormData(),
      str = window.location.href,
      param = '',
      selected = [];
  $('.modal').each(function(){
    $(this).modal('hide');
  });
  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }
  url = url + param;
  var rows_selected = dataTable.column(0).checkboxes.selected();
    console.log("rows_selected : " , rows_selected);
    $.each(rows_selected, function(index, rowId){
        console.log("rowId : " , rowId);
      selected.push(rowId);
    });
    console.log("selected : " , selected);
    if(selected.length == 0){
      toastr.error('Silahkan Pilih Minimal Satu');
    } else {
      var loader = $('.ui.dimmable #main_loader');
      loader.dimmer({closable: false}).dimmer('show');
      formData.append('selected', selected);
      $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
         loader.dimmer({closable: false}).dimmer('hide');
          if(res.rc == 0){
            toastr.success('Sukses');
            loadMenu(res.url);
            // dataTable.ajax.reload();
          } else {
            toastr.error(res.rm);
          }
        },
        error: function(data){
          loader.dimmer({closable: false}).dimmer('hide');
          toastr.error('Terjadi Kesalahan');
        }
      });
    }
}
function sendAll(url){
  var formData = new FormData(),
      str = window.location.href,
      param = '',
      selected = [];
  $('.modal').each(function(){
    $(this).modal('hide');
  });
  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }
  url = url + param;
  var rows_selected = dataTable.column(0).data();
    console.log("rows_selected : " , rows_selected);
    $.each(rows_selected, function(index, rowId){
        console.log("rowId : " , rowId);
      selected.push(rowId);
    });
    console.log("selected : " , selected);
    if(selected.length == 0){
      toastr.error('Silahkan Pilih Minimal Satu');
    } else {
      var loader = $('.ui.dimmable #main_loader');
      loader.dimmer({closable: false}).dimmer('show');
      formData.append('selected', selected);
      $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
         loader.dimmer({closable: false}).dimmer('hide');
          if(res.rc == 0){
            toastr.success('Sukses');
            loadMenu(res.url);
          } else {
            toastr.error(res.rm);
          }
        },
        error: function(data){
          loader.dimmer({closable: false}).dimmer('hide');
          toastr.error('Terjadi Kesalahan');
        }
      });
    }
}

function get(idRoute){
  var str = window.location.href,
      route = typeof idRoute === 'undefined'?$("#routeDelete").val():idRoute,
      url = base_url+ "/" + route ,
      param = '';
  if(str.indexOf('?') > -1){
    param = str.slice(str.indexOf('?'));
  }
  url = url + param;
  $('.modal').each(function(){
        $(this).modal('hide');
  });
   var loader = $('.ui.dimmable #main_loader');
  loader.dimmer({closable: false}).dimmer('show');
  $.get(url, function(response, status, xhr){
      loader.dimmer({closable: false}).dimmer('hide');
      if(response.rc == 0){
        toastr.success('Sukses');
        dataTable.ajax.reload();
      } else {
        toastr.error(response.rm);
      }
    }).fail(function(response) {
      loader.dimmer({closable: false}).dimmer('hide');
      toastr.error('Terjadi Kesalahan');
    });
}

function changeSelect(elm, elmChange, idReff){
  var id = $(elm).val();
  var url = 'refference/'+id+"/"+idReff;
  console.log(elmChange);
  console.log(url);
  var elmReplace = $("select[name='"+elmChange+"']");
  elmReplace.empty();
  elmReplace.parent().addClass('loading');
  $.get(base_url+'/'+url, function(response, status, xhr){
      elmReplace.parent().removeClass('loading');
      console.log(response);
      elmReplace.empty().append(response);
    }).fail(function(response) {
      elmReplace.parent().removeClass('loading');
      console.log(response);
      if(response.status == 401){
        window.location.href = base_url;
      } else {
        toastr.error('Terjadi Kesalahan');
      }          
    });
}
function branchChangeSelect(elm, elmChange, idReff){
  var id = $(elm).val();
  var url = 'refference/'+id+"/"+idReff;
  console.log(elmChange);
  console.log(url);
  var elmReplace = $("select[name='"+elmChange+"']");
  $("select[name='"+elmChange+"'] option[value='1']").each(function() {
          $(this).remove();
  });
  if (id == 1) {
      elmReplace.parent().addClass('disabled');
      var def = '<option value="1" selected>Pusat</option>';
      elmReplace.append(def);
  }else{
      elmReplace.parent().removeClass('disabled');
  }
}
function exportExcel(url, method){
  $('#form').attr('action', url);
  $('#form').attr('method', method);
  console.log($('input[type=search]').val());
  $('#search').val($('input[type=search]').val());
  $('#order').val(dataTable.order());
  $('#form').submit();
}
function upload(url){
  var fileIn = $("#fileToUpload")[0],
      formData = new FormData();
  //Has any file been selected yet?
  if (fileIn.files === undefined || fileIn.files.length == 0) {
      alert("Please select a file");
      return;
  }

  //We will upload only one file in this demo
  var file = fileIn.files[0];
  formData.append('excel_file', fileIn.files[0]);
  //Show the progress bar
  $("#uploadProgress").removeClass('hide');
  $('.message .close')
  .on('click', function() {
    $(this)
      .closest('.message')
      .removeClass('visible')
      .addClass('hidden');
  });
  $("#uploadProgress").progress('reset');
  $.ajax({
      url: base_url + '/' + url,
      type: "POST",
      data: formData,
      processData: false, //Work around #1
      contentType: false, //Work around #2
      success: function(res){
          $("#uploadProgress").addClass('hide');
          if(res.rc == 0){
            toastr.success(res.rm);
            loadMenu(res.url);
          } else {
            dataTable.ajax.reload();
            $('.message').removeClass('hidden');
            $('#errorMsg').empty();
            $('#errorMsg').html(res.rm);
          }
      },
      error: function(res){
        console.log(res);
        alert("Failed");
      },
      xhr: function() {
          myXhr = $.ajaxSettings.xhr();
          if(myXhr.upload){
              myXhr.upload.addEventListener('progress',showProgress, false);
          } else {
              console.log("Upload progress is not supported.");
          }
          return myXhr;
      }
  });
}
function uploadWithForm(url){
  //get value of autonumeric
  if ($(".currency").length>0) {
        $(".currency").each(function(){
          $(this).val($(this).autoNumeric('get'));
        });
    }
  if ($(".percent").length>0) {
      $(".percent").each(function(){
        $(this).val($(this).autoNumeric('get'));
      });
    }

  var fileIn = $("#fileToUpload")[0],
      formData = new FormData($('#dt_form')[0]);
  if (fileIn.files === undefined || fileIn.files.length == 0) {
      alert("Please select a file");
      return;
  }
  var file = fileIn.files[0];
  formData.append('excel_file', fileIn.files[0]);
  // for (var pair of formData.entries()) {
  //       console.log(pair[0]+ ', ' + pair[1]); 
  //   }

  $("#uploadProgress").removeClass('hide');
  $('.message .close')
  .on('click', function() {
    $(this)
      .closest('.message')
      .removeClass('visible')
      .addClass('hidden');
  });
  $("#uploadProgress").progress('reset');
  $.ajax({
      url: base_url + '/' + url,
      type: "POST",
      data: formData,
      processData: false, //Work around #1
      contentType: false, //Work around #2
      success: function(res){
          $("#uploadProgress").addClass('hide');
          if(res.rc == 0){
            toastr.success(res.rm);
            loadMenu(res.url);
          } else {
            $('.message').removeClass('hidden');
            $('#errorMsg').empty();
            $('#errorMsg').html(res.rm);
          }
      },
      error: function(){
       console.log(res);
        alert("Failed");
      },
      xhr: function() {
          myXhr = $.ajaxSettings.xhr();
          if(myXhr.upload){
              myXhr.upload.addEventListener('progress',showProgress, false);
          } else {
              console.log("Upload progress is not supported.");
          }
          return myXhr;
      }
  });
}
function showProgress(evt) {
    if (evt.lengthComputable) {
        var percentComplete = (evt.loaded / evt.total) * 100;
        console.log(percentComplete);
        $('#uploadProgress').progress('increment', percentComplete);
        $("#uploadProgress" + ' .label').text("File Upload..."+percentComplete + "%")
    }
}
function changeJTSewa(elm){
  if($(elm).val() < 3){
    $("input[name='jatuh_tempo_sewa']").prop('disabled', true);
    $("input[name='jatuh_tempo_sewa']").prop('required', false);
  } else {
    $("input[name='jatuh_tempo_sewa']").prop('disabled', false);
    $("input[name='jatuh_tempo_sewa']").prop('required', true);
  }
}
function changeKgtKantor(elm, elmChange){
  var elmReplace = $("select[name='"+elmChange+"']");
  console.log(elmReplace);
  if($(elm).val() == 4){
    elmReplace.parent().parent().removeClass('hide');  
  } else {
    elmReplace.parent().parent().addClass('hide');
  }
}

function changeJarKan(elm, elmChange){
  var elmReplace = $("input[name='"+elmChange+"']");
  if($(elm).val() == 3){
    elmReplace.parent().parent().parent().removeClass('hide');  
  } else {
    elmReplace.parent().parent().parent().addClass('hide');
  }
}
function trxEod(url){
    var loader = $('.ui.dimmable #main_loader');
    loader.dimmer({closable: false}).dimmer('show');
    $.get(base_url+'/'+url, function(response, status, xhr){
        loader.dimmer({closable: false}).dimmer('hide');
        if(response.rc == 0){
            toastr.success(response.rm);
        } else {
            toastr.error(response.rm);
        }
    }).fail(function(response) {
        loader.dimmer({closable: false}).dimmer('hide');
        console.log(response);
        if(response.status == 401){
            window.location.href = base_url;
        } else {
            toastr.error('Terjadi Kesalahan');
        }
    });
}
