<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/20/18
 * Time: 2:03 PM
 */

namespace App\Http\Controllers\Branch;


use App\Http\Controllers\Controller;
use App\Models\BranchModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class BranchController extends Controller
{

    public function index(Request $request){

        $model = (new BranchModel())->all();
        if ($request->ajax()) {
            $view = view('branch.index',['model' => $model])->renderSections();
            return json_encode($view);
        }

        return view('branch.index',['model' => $model]);
    }

    public function seedData(){
        $branch = BranchModel::select('id','branch_name','note');
        return Datatables::of($branch)->addColumn('action', function ($branch){
            return '<button type="button" class="btn btn-primary btn-sm mb-10" id="add_branch" onclick="showEdit('.$branch->id.')"><i class="fa fa-pencil-square"></i> Edit</button>';
        })
            ->make(true);
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'branch_name' => 'required',
            'note' => 'required'
        ]);

        if($validator->fails()){
            return json_encode(['status' => '0', 'msg' => 'Input Gagal', 'data' => $validator->errors()->toJson()]);
        }

        $branch = new BranchModel();
        $branch->branch_name = $request->input('branch_name');
        $branch->note = $request->input('note');

        $branch->save();

        return json_encode(['status' => '1', 'msg' => 'Input Success', 'data' => $branch]);

    }

    public function find($id){
        $branch = DB::select('select * from m_branch where id = :id',['id' => $id]);
        return json_encode($branch);
    }

    public function update(Request $request){
        $branch = BranchModel::find($request->input('branch_id'));

        $branch->branch_name = $request->input('branch_name');
        $branch->note = $request->input('note');
        $branch->save();


        return json_encode(['status' => '1', 'msg' => 'Update Success', 'data' => $branch]);
    }

}