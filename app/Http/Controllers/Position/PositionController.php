<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/20/18
 * Time: 2:03 PM
 */

namespace App\Http\Controllers\Position;


use App\Http\Controllers\Controller;
use App\Models\PositionModel;
use App\Models\UnitModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class PositionController extends Controller
{

    public function index(Request $request){

        $model = (new PositionModel())->all();
        $unit = (new UnitModel())->all();
        if ($request->ajax()) {
            $view = view('position.index',['model' => $model])->renderSections();
            return json_encode($view);
        }

        return view('position.index',['model' => $model, 'unit' => $unit]);
    }

    public function seedData(){
        $position = PositionModel::join('m_unit','m_unit.id', '=', 'm_position.unit_id')->select('m_position.id','m_position.position_name','m_position.note','m_unit.unit_name');
        return Datatables::of($position)->addColumn('action', function ($position){
            return '<button type="button" class="btn btn-primary btn-sm mb-10" onclick="showEdit('.$position->id.')"><i class="fa fa-pencil"></i> Edit</button>';
        })
            ->make(true);
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'position_name' => 'required',
            'note' => 'required',
            'unit_id' => 'required'
        ]);

        if($validator->fails()){
            return json_encode(['status' => '0', 'msg' => 'Input Gagal', 'data' => $validator->errors()->toJson()]);
        }

        $position = new PositionModel();
        $position->position_name = $request->input('position_name');
        $position->note = $request->input('note');
        $position->unit_id = $request->input('unit_id');

        $position->save();

        return json_encode(['status' => '1', 'msg' => 'Input Success', 'data' => $position]);

    }

    public function update(Request $request){
        $position = PositionModel::find($request->input('position_id'));
        $position->position_name = $request->input('position_name');
        $position->note = $request->input('note');
        $position->unit_id = $request->input('unit_id');
        $position->save();
        return json_encode(['status' => '1', 'msg' => 'Input Success', 'data' => $position]);
    }

    public function find($id){
        $position = DB::select('select * from m_position where id = :id',['id' => $id]);
        return json_encode($position);
    }

}