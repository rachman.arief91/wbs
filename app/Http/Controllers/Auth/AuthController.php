<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/20/18
 * Time: 4:32 PM
 */

namespace App\Http\Controllers\Auth;


class AuthController
{
    public function index(\Request $request){
        return view('auth.login');
    }
    public function doLogout(){
        \Auth::logout();

        return \Redirect::to('/login');
    }
}