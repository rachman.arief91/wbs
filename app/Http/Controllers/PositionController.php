<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PositionController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $view = view('position.index')->renderSections();
            return json_encode($view);
        }
        return view('position.index');
    }
}
