<?php

namespace App\Http\Controllers;

use App\Models\ReportModel;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;


class ReportController extends Controller
{
    public function index(){
        return view('report.index');
    }

    public function outgoingReport(){
        return view('report.outgoing');
    }

    public function seedMaster(Request $request){

        $role = \Session::get('user')->role_id;

        $report = ReportModel::select('id','ticket_no','name','alias','email','phone', 'address','attachment','status','created_at')/*->whereIn('status',[0,1])*/;

        if($role == 0){
            $report = $report->where('status','=',0);
        }
        if($role == 1){
            $report = $report->where('status', '=', 1);
        }
        if($role == 2){
            $report = $report->where('status', '=', 2);
        }

        return Datatables::of($report)
            ->addColumn('details', function ($report){
                return url('api/report/detail/'.$report->id);
            })->filter(function ($report) use ($request){
                if($request->has('status')){
                    $report->where('status', '=', "{$request->get('status')}");
                }

                if($request->has('tgl_awal') && $request->has('tgl_akhir')){
                    $report->whereBetween(DB::raw('created_at::timestamp::date'),["{$request->get('tgl_awal')}","{$request->get('tgl_akhir')}"]);
                }

            })->addColumn('status_label', function ($report){
                switch ($report->status){
                    case 0:
                        return '<span class="label bg-default">Laporan Diterima</span>';
                        break;
                    case 1:
                        return '<span class="label bg-cyan">Laporan Diproses</span>';
                        break;
                    case 2:
                        return '<span class="label bg-blue">Usulan Pingroup</span>';
                        break;
                    case 3:
                        return '<span class="label bg-green">Audit Investigasi</span>';
                        break;
                    case 4:
                        return '<span class="label bg-danger">Laporan Ditolak</span>';
                        break;
                }
                return;
            })
            ->addColumn('action', function ($report){
                return '
                            <button type="button" class="btn btn-danger mb-10" onclick="edit('.$report->id.')"><i class="fa fa-leanpub"></i> Tindak Lanjut</button>
                            <a href="'.url('/public/attch/'.$report->attachment).'" download="'.$report->attachment.'" target="_blank" class="btn btn-blue mb-10"><i class="fa fa-download"></i> Attachment</a>
                        
                        ';
            })->rawColumns(['status_label','action'])->make(true);
    }

    public function seedDetail($id){
        $report = ReportModel::select('involved_person','incident_time','incident_loc','chronology','loss','indication','prem_study','pingroup_prop','pindiv_dec')->where('id',$id);
        return Datatables::of($report)->make(true);
    }

    public function seedApprove(){
        $report = ReportModel::select('id','ticket_no','name','email','phone', 'address','attachment')->where(['status' => 2]);
        return Datatables::of($report)
            ->addColumn('details', function ($report){
                return url('api/report/detail/'.$report->id);
            })->addColumn('action', function ($report){
                return '<a href="'.url('/public/attch/'.$report->attachment).'" download="'.$report->attachment.'" target="_blank" class="btn btn-blue mb-10"><i class="fa fa-download"></i> Attachment</a>';
            })->rawColumns(['action'])->make(true);
    }

    public function seedOutgoing(Request $request){
        $role = \Session::get('user')->role_id;
        $report = ReportModel::select('id','ticket_no','name','email','phone', 'address','attachment','status','created_at');

        if($role == 0){
            $report = $report->where('status','>=',1);
        }

        if($role == 1){
            $report = $report->where('status','>=',2);
        }

        if($role == 2){
            $report = $report->where('status','>=',3);
        }

        return Datatables::of($report)
            ->addColumn('details', function ($report){
                return url('api/report/detail/'.$report->id);
            })->addColumn('status_label', function ($report){
                switch ($report->status){
                    case 0:
                        return '<span class="label bg-default">Laporan Diterima</span>';
                        break;
                    case 1:
                        return '<span class="label bg-cyan">Laporan Diproses</span>';
                        break;
                    case 2:
                        return '<span class="label bg-blue">Usulan Pingroup</span>';
                        break;
                    case 3:
                        return '<span class="label bg-green">Audit Investigasi</span>';
                        break;
                    case 4:
                        return '<span class="label bg-danger">Laporan Ditolak</span>';
                        break;
                }
                return;
            })->filter(function ($report) use ($request){
                if($request->has('status')){
                    $report->where('status', '=', "{$request->get('status')}");
                }

                if($request->has('tgl_awal') && $request->has('tgl_akhir')){
                    $report->whereBetween(DB::raw('created_at::timestamp::date'),["{$request->get('tgl_awal')}","{$request->get('tgl_akhir')}"]);
                }

            })->addColumn('action', function ($report){
                return '
                <a href="'.url('/public/attch/'.$report->attachment).'" download="'.$report->attachment.'" target="_blank" class="btn btn-blue mb-10">Attachment &nbsp; <i class="fa fa-download"></i></a>
                <button class="btn btn-dutch btn-ef btn-ef-3 btn-ef-3c" data-dismiss="modal" id="export_detail" onclick="export_detail('.$report->id.')"><i class="fa fa-print"></i> Print</button>
                ';
            })->rawColumns(['action','status_label'])->make(true);
    }

    public function detailModal($id){
        //$data = ReportModel::all()->where('id',$id);
        $data = DB::select('select * from t_report where id = :id', ['id' => $id]);
        return json_encode($data);
    }

    public function updateStatus(Request $request){
        $id = $request->input('id');
        $status = $request->input('status');
        $role_id = \Session::get('user')->role_id;

        $model = ReportModel::find($id);

        if($role_id == 0){
            $model->status = 1;
            $model->prem_study = $request->input('prem_study');
        }

        if($role_id == 1){
            $model->status = 2;
            $model->prem_study = $request->input('prem_study');
            $model->pingroup_prop = $request->input('pingroup_prop');
        }

        if($role_id == 2){
            $model->status = $status;
            $model->prem_study = $request->input('prem_study');
            $model->pingroup_prop = $request->input('pingroup_prop');
            $model->pindiv_dec = $request->input('pindiv_dec');
        }
        $model->save();
        return json_encode($model);
    }

    public function updateReview($id){
        $model = ReportModel::find($id);
        $model->status = 1;
        $model->save();
        return json_encode($model);
    }

    public function doPdf(Request $request){
        $model = DB::select('select created_at, ticket_no, alias, involved_person, action_type, incident_loc, incident_time, indication, chronology, loss, case when status = 0 then \'Laporan Diterima\' when status = 1 then \'Laporan Diproses\' when status = 2 then \'Usulan Pingroup\' when status = 3 then \'Audit Investigasi\' when status = 4 then \'Laporan Ditolak\' end status  from t_report where status <> 0');
        if($request->has('awal') && $request->has('akhir') && $request->has('status')){
            $model = DB::select('select created_at, ticket_no, alias, involved_person, action_type, incident_loc, incident_time, indication, chronology, loss, case when status = 0 then \'Laporan Diterima\' when status = 1 then \'Laporan Diproses\' when status = 2 then \'Usulan Pingroup\' when status = 3 then \'Audit Investigasi\' when status = 4 then \'Laporan Ditolak\' end status  from t_report where created_at::timestamp::date between :tgl_awal and :tgl_akhir and status = :status',['tgl_awal' => $request->get('awal'), 'tgl_akhir' => $request->get('akhir'), 'status' => $request->get('status')]);
        }else if($request->has('status')){
            $model = DB::select('select created_at, ticket_no, alias, involved_person, action_type, incident_loc, incident_time, indication, chronology, loss, case when status = 0 then \'Laporan Diterima\' when status = 1 then \'Laporan Diproses\' when status = 2 then \'Usulan Pingroup\' when status = 3 then \'Audit Investigasi\' when status = 4 then \'Laporan Ditolak\' end status  from t_report where status = :status',['status' => $request->get('status')]);
        } else if($request->has('awal') && $request->has('akhir')){
            $model = DB::select('select created_at, ticket_no, alias, involved_person, action_type, incident_loc, incident_time, indication, chronology, loss, case when status = 0 then \'Laporan Diterima\' when status = 1 then \'Laporan Diproses\' when status = 2 then \'Usulan Pingroup\' when status = 3 then \'Audit Investigasi\' when status = 4 then \'Laporan Ditolak\' end status  from t_report where created_at::timestamp::date between :tgl_awal and :tgl_akhir',['tgl_awal' => $request->get('awal'), 'tgl_akhir' => $request->get('akhir')]);
        }
        $pdf = \PDF::loadView('report.pdf_template',['model' => $model])->setPaper('A4','landscape');
        return $pdf->download('info.pdf');
    }

    public function doPdfDetail($id){
        $model = ReportModel::find($id);
        $pdf = \PDF::loadView('report.pdf_detail',['model' => $model])->setPaper('A4','landscape');
        return $pdf->download('detail.pdf');
    }
}
