<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $view = view('unit.index')->renderSections();
            return json_encode($view);
        }
        return view('unit.index');
    }
}
