<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/21/18
 * Time: 10:16 AM
 */

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Mail\MailAdmin;
use App\Mail\MailCustom;
use App\Models\BranchModel;
use App\Models\ReportModel;
use App\Models\UnitModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function index(Request $request){
        $branch = (new BranchModel())::all();
        if ($request->ajax()) {
            $view = view('frontend.index', ['branch' => $branch])->renderSections();
            return json_encode($view);
        }
        return view('frontend.index', ['branch' => $branch]);
    }

    public function getUnitByBranch($id){
        /*$unit = UnitModel::all()->where('branch_id','=', '2')->toArray();*/
        $unit = DB::select("select id, unit_name from m_unit where branch_id = :id",['id' => $id]);
        return json_encode($unit);
    }

    public function getPositionByUnit($id){
        $position = DB::select("select id, position_name from m_position where unit_id = :id", ['id' => $id]);
        return json_encode($position);
    }

    public function insert(Request $request){

        if($request->hasFile('file')){

            $name = $request->input('name');
            $position = $request->input('position');
            $address = $request->input('address');
            $email = $request->input('email');
            $tlp = $request->input('tlp');
            $involved_person = $request->input('involved_person');
            $action_type = $request->input('action_type');
            $incident_time = $request->input('incident_time');
            $incident_loc = $request->input('incident_loc');
            $indication = $request->input('indication');
            $chronology = $request->input('chronology');
            $loss = $request->input('loss');
            $file = $request->file('file');

            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;

            /*DB::table('t_report')->insert(
                [
                    'name' => $name,
                    'position_id' => $position,
                    'address' => $address,
                    'email' => $email,
                    'phone' => $tlp,
                    'involved_person' => $involved_person,
                    'action_type' => $action_type,
                    'incident_time' => $incident_time,
                    'incident_loc' => $incident_loc,
                    'indication' => $indication,
                    'chronology' => $chronology,
                    'loss' => $loss,
                    'attachment' => $newName,
                    'ip' => $request->getClientIp(),
                    'status' => 1,
                    'ticket_no' => uniqid()
                ]
            );*/

            $report = new ReportModel();

            $report->name = $name;
            $report->position_id = $position;
            $report->address = $address;
            $report->email = $email;
            $report->phone = $tlp;
            $report->involved_person = $involved_person;
            $report->action_type = $action_type;
            $report->incident_time = $incident_time;
            $report->incident_loc = $incident_loc;
            $report->indication = $indication;
            $report->chronology = $chronology;
            $report->note = 'Default';
            $report->loss = $loss;
            $report->attachment = $newName;
            $report->ip = $request->getClientIp();
            $report->status = 0;
            $uuid = uniqid();
            $report->ticket_no = $uuid;
            $report->alias = substr($uuid,0,2).substr($uuid,11,2);

            /*$obj = new \stdClass();
            $obj->no_ticket = $report->ticket_no;
            \Mail::to($report->email)->cc(['ikhwan.nast@bjbs.co.id','atep.roni@bjbs.co.id'])->send(new MailCustom());*/

            $report->save();

            $file->move('public/attch',$newName);

        }else{
            echo "Failure";
        }
    }

    public function getPanduan(Request $request){
        if ($request->ajax()) {
            $view = view('frontend.panduan')->renderSections();
            return json_encode($view);
        }
        return view('frontend.panduan');
    }

    public function getStatus($kode_tiket){
        $tiket = DB::select('select ticket_no, status from t_report where ticket_no = :kode_tiket',['kode_tiket' => $kode_tiket]);

        if($tiket == null){
            return json_encode(['status' => '0', 'msg' => 'Ticket Tidak Ditemukan!']);
        }

        return json_encode(['status' => '1', 'msg' => 'Tiket Ditemukan', 'data' => $tiket]);
    }

    public function test(){

        /*$obj = new \stdClass();
        $obj->no_ticket = 'Hello';

        \Mail::to('ikhwan.nast@bjbs.co.id')->send(new MailCustom($obj));*/
        return Carbon::now()->toDateTimeString();
        //$ori = uniqid();

        //return "ORI: ".$ori." trim: ".substr($ori,0,2).substr($ori,11,2);
    }
}