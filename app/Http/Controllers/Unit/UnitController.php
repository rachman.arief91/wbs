<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/20/18
 * Time: 2:03 PM
 */

namespace App\Http\Controllers\Unit;


use App\Http\Controllers\Controller;
use App\Models\BranchModel;
use App\Models\UnitModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class UnitController extends Controller
{

    public function index(Request $request){

        $model = (new UnitModel())->all();
        $branch = BranchModel::all();
        if ($request->ajax()) {
            $view = view('unit.index',['model' => $model])->renderSections();
            return json_encode($view);
        }

        return view('unit.index',['model' => $model, 'branch' => $branch]);
    }

    public function seedData(){
        $branch = UnitModel::join('m_branch','m_branch.id','=', 'm_unit.branch_id')->select('m_unit.id','m_unit.unit_name','m_unit.note','m_branch.branch_name');
        return Datatables::of($branch)->addColumn('action', function ($branch){
            return '<button type="button" class="btn btn-primary btn-sm mb-10" id="add_unit" onclick="showEdit('.$branch->id.')"><i class="fa fa-pencil-square"></i> Edit</button>';
        })
            ->make(true);
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'unit_name' => 'required',
            'note' => 'required',
            'branch_id' => 'required'
        ]);

        if($validator->fails()){
            return json_encode(['status' => '0', 'msg' => 'Input Gagal', 'data' => $validator->errors()->toJson()]);
        }

        $unit = new UnitModel();
        $unit->unit_name = $request->input('unit_name');
        $unit->note = $request->input('note');
        $unit->branch_id = $request->input('branch_id');

        $unit->save();

        return json_encode(['status' => '1', 'msg' => 'Input Success', 'data' => $unit]);

    }

    public function update(Request $request){
        $unit = UnitModel::find($request->input('unit_id'));
        $unit = UnitModel::find($request->input('unit_id'));
        $unit->unit_name = $request->input('unit_name');
        $unit->note = $request->input('note');
        $unit->branch_id = $request->input('branch_id');
        $unit->save();

        return json_encode(['status' => '1', 'msg' => 'Update Success', 'data' => $unit]);
    }

    public function find($id){
        $unit = DB::select('select * from m_unit where id = :id',['id' => $id]);
        return json_encode($unit);
    }

}