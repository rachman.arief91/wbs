<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/21/18
 * Time: 1:16 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UnitModel extends Model
{
    protected $table = 'm_unit';
    protected $primaryKey = 'id';

}