<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/20/18
 * Time: 2:33 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PositionModel extends Model
{
    protected $table = 'm_position';
    protected $primaryKey = 'id';

}