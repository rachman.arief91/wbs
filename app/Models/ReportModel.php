<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/23/18
 * Time: 2:15 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ReportModel extends Model
{
    protected $table = 't_report';
    protected $primaryKey = 'id';
}