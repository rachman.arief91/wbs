<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 3/20/18
 * Time: 2:33 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class BranchModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'm_branch';
    protected $primaryKey = 'id';

    protected $auditStrict = true;

}