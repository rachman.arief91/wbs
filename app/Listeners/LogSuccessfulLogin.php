<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 4/6/18
 * Time: 12:15 AM
 */

namespace App\Listeners;


use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Login;

class LogSuccessfulLogin
{
    public function __construct()
    {

    }

    public function handle(Login $login){
        $user = User::find($login->user->id);
        $user->last_login = Carbon::now()->toDateTimeString();
        $user->save();
    }
}