<?php
/**
 * Created by PhpStorm.
 * User: rachmanareef
 * Date: 4/6/18
 * Time: 12:15 AM
 */

namespace App\Listeners;


use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Logout;
use Illuminate\Auth\Events\Registered;

class LogSuccessfulLogout
{
    public function __construct()
    {

    }

    public function handle(Logout $logout){
        $user = User::find($logout->user->id);
        $user->last_logout = Carbon::now()->toDateTimeString();
        $user->save();
    }
}