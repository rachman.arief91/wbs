<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_report', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('alias')->nullable();
            $table->integer('position_id')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->string('attachment')->nullable();
            $table->string('ip')->nullable();
            $table->string('note')->nullable();
            $table->integer('status')->nullable();
            $table->text('involved_person')->nullable();
            $table->text('action_type')->nullable();
            $table->timestamp('incident_time')->nullable();
            $table->text('incident_loc')->nullable();
            $table->text('chronology')->nullable();
            $table->text('loss')->nullable();
            $table->string('ticket_no')->nullable();
            $table->text('indication')->nullable();
            $table->text('prem_study')->nullable();
            $table->text('pingroup_prop')->nullable();
            $table->text('pindiv_dec')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_report');
    }
}
