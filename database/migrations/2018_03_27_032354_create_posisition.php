<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosisition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_position', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position_name');
            $table->string('note');
            $table->integer('unit_id');
            //$table->foreign('unit_id')->references('id')->on('m_unit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_posisition');
    }
}
