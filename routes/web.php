<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('login', 'Auth\LoginController@doLogin');

Route::get('/frontend', 'Frontend\FrontendController@index');
Route::get('/panduan','Frontend\FrontendController@getPanduan');
Route::get('/frontend/unit/{id}','Frontend\FrontendController@getUnitByBranch');
Route::get('/frontend/position/{id}','Frontend\FrontendController@getPositionByUnit');
Route::post('/frontend/save','Frontend\FrontendController@insert');
Route::get('/frontend/ticket/{id}', 'Frontend\FrontendController@getStatus');
Route::get('/frontend/test', 'Frontend\FrontendController@test');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/branch', 'Branch\BranchController@index');
    Route::get('/branch/{id}', 'Branch\BranchController@find');
    Route::post('/branch/update','Branch\BranchController@update');

    Route::get('/unit', 'Unit\UnitController@index');
    Route::get('/unit/{id}','Unit\UnitController@find');
    Route::post('/unit/update','Unit\UnitController@update');

    Route::get('/position', 'Position\PositionController@index');
    Route::get('/position/{id}','Position\PositionController@find');
    Route::post('/position/update','Position\PositionController@update');

    Route::get('/report', 'ReportController@index');
    Route::get('/outgoing','ReportController@outgoingReport');

    Route::get('/api/report/master', 'ReportController@seedMaster');
    Route::get('/api/report/detail/{id}', 'ReportController@seedDetail');

    Route::get('/api/report/outgoing','ReportController@seedOutgoing');

    Route::get('/api/report/modal/{id}', 'ReportController@detailModal');
    Route::post('/api/report/update', 'ReportController@updateStatus');
    Route::get('/api/report/update/review/{id}', 'ReportController@updateReview');


    Route::get('/api/branch','Branch\BranchController@seedData');
    Route::post('/api/branch/store', 'Branch\BranchController@store');

    Route::get('/api/unit','Unit\UnitController@seedData');
    Route::post('/api/unit/store', 'Unit\UnitController@store');

    Route::get('/api/position','Position\PositionController@seedData');
    Route::post('/api/position/store', 'Position\PositionController@store');

    Route::get('/pdf/download','ReportController@doPdf');
    Route::get('/pdf/detail/{ticket_no}', 'ReportController@doPdfDetail');
});


