<p>Assalamualaikum Warahmatullahi Wabarakatuh</p>
<p>Terimakasih atas laporan yang telah disampaikan dengan Nomor Tiket: </p>
<div style="text-align: center">
    <p>{{ $demo->no_ticket }}</p>
</div>
<p>Laporan tersebut akan segera ditindaklanjuti sesuai ketentuan yang berlaku. Bila diperlukan data, informasi, dan dokumen tambahan, kami akan menghubungi Anda. Selanjutnya untuk memantau progress tindaklanjut
    laporan yang telah disampaikan, dapat diketahui dengan mengakses portal <a href="http://172.31.251.205/wbs/login">ini</a> dengan memasukan nomor tiket pada kolom isian Progress Tindak Lanjut
</p>
<p>Demikian Terimakasih</p>

<p>Waalaikumsalam Warahmatullahi Wabarakatuh</p>

</br>
</br>

<p>Pengelola <br> Bank BJB Syariah</p>