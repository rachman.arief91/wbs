<p>Assalamualaikum Warahmatullahi Wabarakatuh</p>
<p>Laporan pengungkapan kejadian indikasi fraud telah diterima dengan Nomor Tiket:</p>
<div style="text-align: center">
    <p>{{ $demo->no_ticket }}</p>
</div>

<p>Pengelola agar segera menindaklanjuti laporan tersebut sesuai ketentuan yang berlaku</p>

<p>Waalaikumsalam Warahmatullahi Wabarakatuh</p>
