@extends('admin.main')

@section('content')
    <style>
        td.details-control {
            background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
    </style>
    <section class="tile">
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong>Laporan Keluar</strong></h1>
        </div>
        <div class="tile-footer text-left bg-tr-black lter dvd dvd-top mb-10">
            <div class="ml-20">
                <div id="tableTools">
                    <button type="button" class="btn btn-cyan btn-sm mb-10" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh Report</button>
                </div>
            </div>
        </div>

        <form id="search_form" role="form" class="form-horizontal" method="POST">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tgl Awal</label>
                <div class="col-sm-4">
                    <div class='input-group '>
                        <input id="f_awal" type='text' class="form-control" name="f_awal" data-parsley-trigger="change" readonly/>
                        <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tgl Akhir</label>
                <div class="col-sm-4">
                    <div class='input-group '>
                        <input id="f_akhir" type='text' class="form-control" name="f_akhir" data-parsley-trigger="change" readonly/>
                        <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="branch" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-4">
                    @php($role = Session::get('user')->role_id)
                    <select name="status" id="f_status" data-parsley-trigger="change" class="form-control mb-10">
                        <option value="">Pilih Status</option>
                        @if($role == 0)
                            <option value="1">Laporan Diproses</option>
                            <option value="2">Usulan Pingroup</option>
                            <option value="3">Audit Investigasi</option>
                            <option value="4">Laporan Ditolak</option>
                        @endif

                        @if($role == 1)
                            <option value="2">Usulan Pingroup</option>
                            <option value="3">Audit Investigasi</option>
                            <option value="4">Laporan Ditolak</option>
                        @endif

                        @if($role == 2)
                            <option value="3">Audit Investigasi</option>
                            <option value="4">Laporan Ditolak</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <button id="search" class="btn btn-blue btn-sm mb-10" type="submit"><i class="fa fa-binoculars"></i> Search</button>
                    <button type="button" class="btn btn-green btn-sm mb-10" onclick="reset()"><i class="fa fa-refresh"></i> Reset Filter</button>
                    <button id="export" class="btn btn-blue btn-sm mb-10" type="button"><i class="fa fa-file-pdf-o"></i> Export</button>
                    {{--<button id="send" type="submit" class="btn btn-rounded btn-primary btn-sm">Kirim</button>--}}
                </div>
            </div>
        </form>

        <div class="tile-body ml-20">
            <div class="table">
                <form id="form" role="form">
                    {{csrf_field()}}
                    <input type="hidden" name="search" id="search">
                    <input type="hidden" name="order" id="order">
                </form>
                <table class="ui striped padded celled table standard-usage" id="tbl_report" width="100%">
                    <thead class="full-width single line">
                    <tr>
                        <th>Review</th>
                        <th style="width: 20px;" class="no-sort collapsing center aligned">No Ticket</th>
                        <th style="width: 40px;" class="no-sort">Pelapor</th>
                        <th style="width: 40px;" class="no-sort">Email</th>
                        <th style="width: 40px;" class="no-sort">Phone</th>
                        <th style="width: 40px;" class="no-sort">Alamat</th>
                        <th style="width: 40px;" class="no-sort">Tanggal</th>
                        <th style="width: 40px;" class="no-sort">Status</th>
                        <th style="width: 40px;" class="no-sort">Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@include('handlebars.detail-template')
@include('modal.m_edit_report')
@section('tablereport')
    <script type="text/javascript">
        $('#f_awal').datetimepicker({
            format: 'YYYY-MM-DD',
            ignoreReadonly: true
        });
        $('#f_akhir').datetimepicker({
            format: 'YYYY-MM-DD',
            ignoreReadonly: true
        });
        var template = Handlebars.compile($("#details-template").html());
        var t_master = $('#tbl_report').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                url: 'api/report/outgoing',
                data: function (d) {
                    d.tgl_awal = $('#f_awal').val();
                    d.tgl_akhir = $('#f_akhir').val();
                    d.status = $('#f_status').val();
                }
            },
            columns: [
                {
                    'className':      'details-control',
                    'orderable':      false,
                    'searchable':      false,
                    'data':           null,
                    'defaultContent': '',
                    'width': '10%',
                },
                {data: 'ticket_no', name: 'ticket_no'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'address', name: 'address'},
                {data: 'created_at', name: 'created_at'},
                {data: 'status_label', name: 'status_label', searchable: false, orderable: false},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
            order: [[1, 'asc']]
        });

        $('#tbl_report tbody').on('click','td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = t_master.row(tr);

            /*$.ajax({
                type: 'GET',
                url: 'api/report/update/review/'+row.data().id,
                success: function (res) {
                    console.log(res);
                }
            });*/



            var tableId = 'detail-' + row.data().id;
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            }else{
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching: false,
                bInfo: false,
                ajax: data.details,
                columns: [
                    { data: 'involved_person', name: 'involved_person' },
                    { data: 'incident_time', name: 'incident_time' },
                    { data: 'incident_loc', name: 'incident_loc' },
                    { data: 'chronology', name: 'chronology' },
                    { data: 'loss', name: 'loss' },
                    { data: 'indication', name: 'indication' },
                    /*{ data: 'prem_study', name: 'prem_study' },
                    { data: 'pingroup_prop', name: 'pingroup_prop' },
                    { data: 'pindiv_dec', name: 'pindiv_dec' }*/
                ]
            })
        }

        function refresh() {
            t_master.ajax.reload();
        }

        $('#search_form').on('submit', function (e) {
            console.log('echo');
            e.preventDefault();
            t_master.draw();

        });

        $('#export').on('click', function (e) {
            var awal   = $('#f_awal').val();
            var akhir  = $('#f_akhir').val();
            var status = $('#f_status').val();
            window.location = 'pdf/download?status='+status+'&awal='+awal+'&akhir='+akhir;
        });

        function export_detail(report_id) {
            window.location = 'pdf/detail/'+report_id;
        }
    </script>

@endsection