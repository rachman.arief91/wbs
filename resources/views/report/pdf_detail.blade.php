<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    @include('report.head')
    <style type="text/css">

    </style>
</head>
<body>
<div class="col-md-5">
    <section class="tile">
        <img class="pull-right" src="<?php echo e(url('images/logo_last.png'))?>">
        <h3 class="custom-font"><strong>DETAIL LAPORAN KASUS</strong></h3>
        <h4 class="custom-font"><strong>DATA PELAPOR</strong></h4>
        <dl class="dl-horizontal filled" style="color: black">
            <dt>No Tiket</dt>
            <dd>{{ $model->ticket_no }}</dd>
            <dt>Nama Samaran</dt>
            <dd>{{ $model->alias }}</dd>
            <dt>Email</dt>
            <dd>{{ $model->email }}</dd>
            <dt>Telpon</dt>
            <dd>{{ $model->phone }}</dd>
            <dt>Tanggal Laporan</dt>
            <dd>{{ $model->created_at }}</dd>
            <dt>Pihak Terlibat</dt>
            <dd><div align="justify">{{ $model->involved_person }}</div></dd>
            <dt>Waktu Kejadian</dt>
            <dd>{{ $model->incident_time }}</dd>
            <dt>Lokasi Kejadian</dt>
            <dd><div align="justify">{{ $model->incident_loc }}</div></dd>
            <dt>Kronologi</dt>
            <dd><div align="justify">{{ $model->chronology }}</div></dd>
            <dt>Indikasi Kerugian</dt>
            <dd><div align="justify">{{ $model->loss }}</div></dd>
            <dt>Motif</dt>
            <dd><div align="justify">{{ $model->action_type }}</div></dd>
        </dl>

        <h4 class="custom-font"><strong>DATA TINDAK LANJUT</strong></h4>
        <dl class="dl-horizontal filled" style="color: black">
            <dt>Analisis Awal</dt>
            <dd><div align="justify">{{ $model->prem_study }}</div></dd>
            <dt>Usulan PinGroup</dt>
            <dd><div align="justify">{{ $model->pingroup_prop }}</div></dd>
            <dt>Keputusan PinDiv</dt>
            <dd><div align="justify">{{ $model->pindiv_dec }}</div></dd>
        </dl>


    </section>

</div>

</body>
</html>