@extends('admin.main')

@section('content')
    <style>
        td.details-control {
            background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
    </style>
    <section class="tile">
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong>Laporan Investigasi</strong></h1>
        </div>
        <div class="tile-footer text-left bg-tr-black lter dvd dvd-top mb-10">
            <div class="ml-20">
                <div id="tableTools">
                    <button type="button" class="btn btn-primary btn-sm mb-10" id="i_btn_add"><i class="fa fa-file-pdf-o"></i> Export</button>
                </div>
            </div>
        </div>
        <div class="tile-body ml-20">
            <div class="table">
                <form id="form" role="form">
                    {{csrf_field()}}
                    <input type="hidden" name="search" id="search">
                    <input type="hidden" name="order" id="order">
                </form>
                <table class="ui striped padded celled table standard-usage" id="tbl_report" width="100%">
                    <thead class="full-width single line">
                    <tr>
                        <th>Review</th>
                        <th style="width: 20px;" class="no-sort collapsing center aligned">No Ticket</th>
                        <th style="width: 40px;" class="no-sort">Pelapor</th>
                        <th style="width: 40px;" class="no-sort">Email</th>
                        <th style="width: 40px;" class="no-sort">Phone</th>
                        <th style="width: 40px;" class="no-sort">Alamat</th>
                        <th style="width: 40px;" class="no-sort">Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@include('handlebars.detail-template')
@include('modal.m_edit_report')
@section('tablereport')
    <script type="text/javascript">
        var template = Handlebars.compile($("#details-template").html());
        var t_master = $('#tbl_report').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'api/report/approve',
            columns: [
                {
                    'className':      'details-control',
                    'orderable':      false,
                    'searchable':      false,
                    'data':           null,
                    'defaultContent': '',
                    'width': '10%',
                },
                {data: 'ticket_no', name: 'ticket_no'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'address', name: 'address'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
            order: [[1, 'asc']]
        });

        $('#tbl_report tbody').on('click','td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = t_master.row(tr);

            /*$.ajax({
                type: 'GET',
                url: 'api/report/update/review/'+row.data().id,
                success: function (res) {
                    console.log(res);
                }
            });*/



            var tableId = 'detail-' + row.data().id;
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            }else{
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching: false,
                bInfo: false,
                ajax: data.details,
                columns: [
                    { data: 'involved_person', name: 'involved_person' },
                    { data: 'incident_time', name: 'incident_time' },
                    { data: 'incident_loc', name: 'incident_loc' },
                    { data: 'chronology', name: 'chronology' },
                    { data: 'loss', name: 'loss' },
                    { data: 'indication', name: 'indication' },
                ]
            })
        }
    </script>

@endsection