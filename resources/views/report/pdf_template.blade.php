<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    @include('report.head')
    <style type="text/css">
        .table th{
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-md-12">

            <section class="tile">
                <img class="pull-right" src="<?php echo e(url('images/logo_last.png'))?>" style="width: 80px; height: 40px">
                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font"><strong>DATA LAPORAN PENGUNGKAPAN KEJADIAN INDIKASI FRAUD</strong></h1>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body p-0">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pelaporan</th>
                            <th>No Ticket</th>
                            <th>Nama Samaran</th>
                            <th>Pihak Terlibat</th>
                            <th>Jenis Fraud</th>
                            <th>Lokasi Kejadian</th>
                            <th>Waktu Kejadian</th>
                            <th>Motif / Indikasi</th>
                            <th>Kronologi</th>
                            <th>Indikasi Kerugian</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 0)
                        @foreach($model as $value)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $value->created_at }}</td>
                                <td>{{ $value->ticket_no }}</td>
                                <td>{{ $value->alias }}</td>
                                <td>{{ $value->involved_person }}</td>
                                <td>{{ $value->action_type }}</td>
                                <td>{{ $value->incident_loc }}</td>
                                <td>{{ $value->incident_time }}</td>
                                <td>{{ $value->indication }}</td>
                                <td>{{ $value->chronology }}</td>
                                <td>{{ $value->loss }}</td>
                                <td>{{ $value->status }}</td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>

                </div>
                <!-- /tile body -->

            </section>
        </div>
    </div>
</body>
</html>