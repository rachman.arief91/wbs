@extends('admin.main')

@section('content')
    <style>
        td.details-control {
            background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }
        .modal{
            max-height:80%;
        }
        .modal-header{
            height:15% !important;
        }
        .modal-body{
            height:70%;
            overflow:auto;
        }
        .modal-footer{
            height:15%;
        }
    </style>
    <section class="tile">
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong>Laporan Masuk</strong></h1>
        </div>
        <div class="tile-footer text-left bg-tr-black lter dvd dvd-top mb-10">
            <div class="ml-20">
                <div id="tableTools">
                    <button type="button" class="btn btn-cyan btn-sm mb-10" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh Report</button>
                </div>
            </div>
        </div>
        <form id="search_form" role="form" class="form-horizontal" method="POST">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal Awal</label>
                <div class="col-sm-4">
                    <div class='input-group '>
                        <input id="f_awal" type='text' class="form-control" name="f_awal" data-parsley-trigger="change" readonly/>
                        <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal Akhir</label>
                <div class="col-sm-4">
                    <div class='input-group '>
                        <input id="f_akhir" type='text' class="form-control" name="f_akhir" data-parsley-trigger="change" readonly/>
                        <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    <button id="search" class="btn btn-blue btn-sm mb-10" type="submit"><i class="fa fa-binoculars"></i> Search</button>
                    <button type="button" class="btn btn-green btn-sm mb-10" onclick="reset()"><i class="fa fa-refresh"></i> Reset Filter</button>
                </div>
            </div>
        </form>
        <div class="tile-body ml-20">
            <div class="table">
                <table class="ui striped padded celled table standard-usage" id="tbl_report" width="100%">
                    <thead class="full-width single line">
                    <tr>
                        <th>Review</th>
                        <th style="width: 20px;" class="no-sort collapsing center aligned">No Ticket</th>
                        <th style="width: 40px;" class="no-sort">Pelapor</th>
                        <th style="width: 40px;" class="no-sort">Alias</th>
                        <th style="width: 40px;" class="no-sort">Email</th>
                        <th style="width: 40px;" class="no-sort">Phone</th>
                        <th style="width: 40px;" class="no-sort">Alamat</th>
                        <th style="width: 40px;" class="no-sort">Tanggal</th>
                        <th style="width: 40px;" class="no-sort">Status</th>
                        <th style="width: 40px;" class="no-sort">Status Hidden</th>
                        <th style="width: 40px;" class="no-sort">Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </section>
@endsection

@include('handlebars.detail-template')
@include('modal.m_edit_report')
@section('tablereport')
    <script type="text/javascript">

        $('#f_awal').datetimepicker({
            format: 'YYYY-MM-DD',
            ignoreReadonly: true
        });
        $('#f_akhir').datetimepicker({
            format: 'YYYY-MM-DD',
            ignoreReadonly: true
        });

        var template = Handlebars.compile($("#details-template").html());
        var t_master = $('#tbl_report').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: 'api/report/master',
                data: function (d) {
                    d.tgl_awal = $('#f_awal').val();
                    d.tgl_akhir = $('#f_akhir').val();
                    d.status = $('#f_status').val();
                }
            },
            columns: [
                {
                    'className':      'details-control',
                    'orderable':      false,
                    'searchable':      false,
                    'data':           null,
                    'defaultContent': '',
                    'width': '10%'
                },
                {data: 'ticket_no', name: 'ticket_no'},
                {data: 'name', name: 'name'},
                {data: 'alias', name: 'alias'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'address', name: 'address'},
                {data: 'created_at', name: 'created_at'},
                {data: 'status_label', name: 'status_label', searchable: false, orderable: false},
                {data: 'status', name: 'status', visible: false},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ],
            order: [[1, 'asc']]
        });


        $('#tbl_report tbody').on('click','td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = t_master.row(tr);

            /*if(row.data().status == 1 || row.data().status == 0){
                $.ajax({
                    type: 'GET',
                    url: 'api/report/update/review/'+row.data().id,
                    success: function (res) {
                        console.log(res);
                    }
                });
            }*/

            var tableId = 'detail-' + row.data().id;
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            }else{
                row.child(template(row.data())).show();
                initTable(tableId, row.data());
                tr.addClass('shown');
                tr.next().find('td').addClass('no-padding bg-gray');
            }
        });

        function initTable(tableId, data) {
            $('#' + tableId).DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching: false,
                bInfo: false,
                ajax: data.details,
                columns: [
                    { data: 'involved_person', name: 'involved_person' },
                    { data: 'incident_time', name: 'incident_time' },
                    { data: 'incident_loc', name: 'incident_loc' },
                    { data: 'chronology', name: 'chronology' },
                    { data: 'loss', name: 'loss' },
                    { data: 'indication', name: 'indication' },
                    { data: 'prem_study', name: 'prem_study' },
                    { data: 'pingroup_prop', name: 'pingroup_prop' },
                    { data: 'pindiv_dec', name: 'pindiv_dec' }
                ]
            })
        }

        var modal = $('#m_edit_report');
        var formupdate = $('#report_edit');

        function edit(id) {
            $.ajax({
                type: 'GET',
                url: 'api/report/modal/'+id,
                success: function (res) {
                    var data = $.parseJSON(res);
                    //console.log(data[0].name)
                    //var data = $.parseJSON(res);
                    $('#d_name').append(data[0].name);
                    $('#d_email').append(data[0].email);
                    $('#d_telp').append(data[0].phone);
                    $('#d_ticket').append(data[0].ticket_no);
                    $('#d_date').append(data[0].created_at);
                    $('#report_id').val(id);
                    $('#prem_study').val(data[0].prem_study);
                    $('#pingroup_prop').val(data[0].pingroup_prop);
                    $('#pindiv_dec').val(data[0].pindiv_dec);
                }
            });
            modal.show('show');
        }

        function updateStatus() {
            formupdate.parsley().validate();
            if(formupdate.parsley().isValid()){
                var dataReport = new FormData(formupdate[0]);
                processUpdate(dataReport);
                toastr.success('Update Laporan Berhasil');
                modal.hide('hide');
            }
        }

        function closeModal() {
            clear();
            modal.hide('hide');
        }

        function processUpdate(data) {
            $.ajax({
                type: 'POST',
                url: 'api/report/update',
                data: data,
                processData: false,
                contentType: false,
                success: function (res) {
                    t_master.ajax.reload();
                    console.log(res);
                }
            });
        }
        
        function refresh() {
            t_master.ajax.reload();
        }

        $('#search_form').on('submit', function (e) {
            console.log('echo');
            e.preventDefault();
            t_master.draw();

        });

        $('#export').on('click', function (e) {
           var awal   = $('#f_awal').val();
           var akhir  = $('#f_akhir').val();
           var status = $('#f_status').val();
            window.location = 'pdf/download?status='+status+'&awal='+awal+'&akhir='+akhir;
        });


        function reset() {
            $('#f_awal').empty();
            $('#f_akhir').empty();
            $('#f_status').empty();
        }
        function clear() {
            $('#d_name').empty();
            $('#d_email').empty();
            $('#d_telp').empty();
            $('#d_ticket').empty();
            $('#d_date').empty();
        }
    </script>

@endsection