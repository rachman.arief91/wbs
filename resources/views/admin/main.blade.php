<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('admin.head')
</head>
<body id="minovate" class="appWrapper">
<div id="wrap" class="animsition" style="height:100%;">
    @include('layouts.header')
    <div id="controls">
        @include('admin.left')
    </div>
    <section>
        <div id="content" class="ui dimmable">
            <div class="ui dimmer" id="main_loader">
                <div class="ui loader"></div>
            </div>

            {{--<div id="data_content">--}}
                @yield('content')
            {{--</div>--}}
        </div>
    </section>
</div>
{{--<div id="data_modal">@include('modal.m_add_branch')</div>--}}
@include('admin.script')
</body>
</html>
