<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

<link rel="stylesheet" href="{{url('css/vendor/bootstrap.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/animate.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/simple-line-icons.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/animsition/animsition.min.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/datatables/semantic.css')}}">
<link rel="stylesheet" href="{{url('css/ui/calendar.min.css')}}">
<link rel="stylesheet" href="{{url('css/jquery.bootstrap-touchspin.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/datatables/dataTables.semanticui.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/datatables/dataTables.checkboxes.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/datatables/rowGroup.semanticui.min.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/daterangepicker/daterangepicker-bs3.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/datetimepicker/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{url('css/vendor/toastr/toastr.min.css')}}">
<link rel="stylesheet" href="{{url('css/main.css')}}">
<script src="{{url('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
<title>Admin - WBS</title>