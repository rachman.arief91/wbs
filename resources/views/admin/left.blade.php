@php($role = Session::get('user')->role_id)
<aside id="sidebar">
    <div id="sidebar-wrap">
        <div class="panel-group slim-scroll" role="tablist">
            <div class="panel panel-default">
                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">
                        <ul id="navigation">
                            <li>
                                <a role="button" tabindex="0"><i class="fa fa-database"></i> <span>Master Data</span></a>
                                <ul>
                                    <li class=""><a href="{!! url('/branch') !!}"><i class="fa fa-tree"></i><span>Cabang</span></a></li>
                                    <li class=""><a href="{!! url('/unit') !!}"><i class="fa fa-gears"></i><span>Unit Kerja</span></a></li>
                                    <li class=""><a href="{!! url('/position') !!}" ><i class="fa fa-user"></i><span>Jabatan</span></a></li>
                                </ul>
                            </li>
                            <li>
                                <a role="button" tabindex="0"><i class="fa fa-table"></i> <span>Laporan</span></a>
                                <ul>
                                    <li class=""><a href="{!! url('report') !!}"><i class="fa fa-inbox"></i>Laporan Masuk</a></li>
                                    <li class=""><a href="{!! url('outgoing') !!}"><i class="fa fa-upload"></i>Laporan Keluar</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>