<script src="{{url('js/jquery.js')}}"></script>
<script src="{{url('js/vendor/moment.min.js')}}"></script>
<script src="{{url('js/vendor/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{url('js/vendor/chosen.jquery.min.js')}}"></script>
<script src="{{url('js/vendor/bootstrap.min.js')}}"></script>
<script src="{{url('js/vendor/jRespond.min.js')}}"></script>
<script src="{{url('js/vendor/jquery.sparkline.min.js')}}"></script>
<script src="{{url('js/vendor/jquery.slimscroll.min.js')}}"></script>
<script src="{{url('js/vendor/jquery.animsition.min.js')}}"></script>
<script src="{{url('js/vendor/screenfull.min.js')}}"></script>
<script src="{{url('js/vendor/parsley.min.js')}}"></script>
<script src="{{url('js/vendor/toastr.min.js')}}"></script>
<script src="{{url('js/jquery.bootstrap-touchspin.min.js')}}"></script>
<script src="{{url('lan_id/parsley/id.js')}}"></script>
<script src="{{url('lan_id/parsley/id_extra.js')}}"></script>
<script src="{{url('js/vendor/jquery.dataTables.min.js')}}"></script>
<script src="{{url('js/vendor/dataTables.semanticui.min.js')}}"></script>
<script src="{{url('js/vendor/semantic.min.js')}}"></script>
<script src="{{url('js/ui/calendar.min.js')}}"></script>
<script src="{{url('js/vendor/dataTables.checkboxes.min.js')}}"></script>
<script src="{{url('js/vendor/dataTables.rowGroup.min.js')}}"></script>
<script src="{{url('js/vendor/dataTables.rowsGroup.js')}}"></script>
<script src="{{url('js/jquery.md5.min.js')}}"></script>
<script src="{{url('js/autoNumeric.js')}}"></script>
<script src="{{url('js/var.js')}}"></script>
<script src="{{url('js/minovate.js')}}"></script>
<script src="{{url('js/function.js')}}"></script>
<script src="{{url('js/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{url('js/handlebars.js')}}"></script>
@yield('tablebranch')
@yield('tablereport')
@yield('tableunit')
@yield('tableposition')