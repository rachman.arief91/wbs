<div class="row">
    <div class="col-md-12">
        <section class="tile">
            <div class="tile-body">
                <form method='post' action="{{--{{ route('refcabang.store') }}--}}" class="form-horizontal" id="m_form_edit_position" data-parsley-validate="">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Position Name </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="e_position_name" name="position_name" data-parsley-trigger="change"
                                   required data-parsley-minlength="4">
                            {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Deskripsi </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="e_position_note" name="note" data-parsley-trigger="change"
                                   required data-parsley-minlength="4">
                            {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="branch" class="col-sm-2 control-label">Unit</label>
                        <div class="col-sm-10">
                            <select name="unit_id" id="e_position_unit" data-parsley-trigger="change" required class="form-control mb-10">
                                <option value="">Pilih Unit...</option>
                                @foreach( $unit as $value)
                                    <option value="{{ $value->id }}">{{ $value->unit_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" id="e_position_id" name="position_id"/>
                </form>
            </div>
        </section>
    </div>
</div>