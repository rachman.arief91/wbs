@extends('admin.main')

@section('content')


    <section class="tile">
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong>Jabatan</strong></h1>
        </div>
        <div class="tile-footer text-left bg-tr-black lter dvd dvd-top mb-10">
            <div class="ml-20">
                <div id="tableTools">
                    <button type="button" class="btn btn-primary btn-sm mb-10" id="i_btn_add" onclick="showAdd()"><i class="fa fa-plus"></i> Tambah Jabatan</button>
                </div>
            </div>
        </div>
        <div class="tile-body ml-20">

            <div class="table">
                <form id="form" role="form">
                    {{csrf_field()}}
                    <input type="hidden" name="search" id="search">
                    <input type="hidden" name="order" id="order">
                </form>
                <table class="ui striped padded celled table standard-usage" id="tbl_position" width="100%">
                    <thead class="full-width single line">
                    <tr>
                        <th style="width: 5%;" class="no-sort collapsing center aligned">No</th>
                        <th style="width: 40%;" class="no-sort">Jabatan</th>
                        <th style="width: 30%;" class="no-sort">Deskripsi</th>
                        <th style="width: 20%;" class="no-sort single line">Unit Kerja</th>
                        <th style="width: 5%;" class="no-sort single line">Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </section>


    {{-- </div>
 </div>
--}}

@endsection

@include('modal.m_add_position')
@include('modal.m_edit_position')

@section('tableposition')
    <script type="text/javascript">

        var modal = $('#m_add_position');
        var modal_upd = $('#m_edit_position');

        var position_tbl = $('#tbl_position').DataTable({
            serverSide: true,
            pagingType: 'full_numbers',
            ajax: {
                url: 'api/position',
                type: 'GET'
            },
            columns: [
                {data: 'id', name: 'm_position.id' },
                {data: 'position_name', name: 'm_position.position_name'},
                {data: 'note', name: 'm_position.note'},
                {data: 'unit_name', name: 'm_unit.unit_name'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ]
        });

        function processInsert () {
            var form = $('#m_form_add_position');
            form.parsley().validate();
            if(form.parsley().isValid()){
                var dataReport = new FormData(form[0]);
                add('api/position/store', dataReport);
                form[0].reset();
                modal.hide('hide');
            }
        }

        function showAdd() {
            modal.show('show');
        }

        function showEdit(id) {
            $.ajax({
                type: 'GET',
                url: 'position/'+id,
                success: function (res) {
                    var data = $.parseJSON(res);
                    $('#e_position_name').val(data[0].position_name);
                    $('#e_position_note').val(data[0].note);
                    $('#e_position_unit').val(data[0].unit_id);
                }
            });
            $('#e_position_id').val(id);
            modal_upd.show('show');
        }
        
        function processUpdate() {
            var form = $('#m_form_edit_position');
            form.parsley().validate();
            if(form.parsley().isValid()){
                var dataUpdate = new FormData(form[0]);
                edit('position/update', dataUpdate);
                modal_upd.hide('hide');
            }
        }

        function closeModal() {
            modal.hide('hide');
        }
        function closeEditModal() {
            modal_upd.hide('hide');
        }

        function edit(url, formdata) {
            $.ajax({
                type: 'POST',
                url: url,
                data: formdata,
                processData: false,
                contentType: false,
                success: function (res) {
                    var data = $.parseJSON(res)
                    if (data.status == 1) {
                        toastr.success("Update Data Sukses!");
                    }
                    if (data.status == 0) {
                        toastr.error("Terjadi Kesalahan");
                    }
                    position_tbl.ajax.reload();
                }
            });
        }

        function add(url, formdata) {
            $.ajax({
                type: 'POST',
                url: url,
                data: formdata,
                processData: false,
                contentType: false,
                success: function (res) {
                    var data = $.parseJSON(res)
                    if(data.status == 1){
                        toastr.success("Input Data Sukses!");
                    }
                    if(data.status == 0){
                        toastr.error("Terjadi Kesalahan");
                    }

                    position_tbl.ajax.reload();
                }
            });
        }

    </script>
@endsection