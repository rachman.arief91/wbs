<script id="details-template" type="text/x-handlebars-template">
    <div class="table">
        <table class="table details-table" id="detail-{{id}}" style="padding-left:50px;">
            <thead>
            <tr>
                <th>Pihak Terlibat</th>
                <th>Waktu Kejadian</th>
                <th>Lokasi Kejadian</th>
                <th>Kronologi</th>
                <th>Indikasi Kerugian</th>
                <th>Motif/Indikasi</th>
                <!--<th>Analisis Awal</th>
                <th>Usulan Pingroup</th>
                <th>Keputusan Pindiv</th>-->
            </tr>
            </thead>
        </table>
    </div>
</script>