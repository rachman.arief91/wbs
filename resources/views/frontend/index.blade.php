@extends('layouts.app')

@section('content')
    <div class="page page-forms-common">
        <div class="row">
            <div class="col-md-6">
                <section class="tile">
                    <div class="tile-header dvd dvd-btm">
                        <h1 class="custom-font"><strong>Data Pelapor</strong></h1>
                    </div>
                    <div class="tile-body">
                        <form id="report" enctype="multipart/form-data" name="report" class="form-horizontal" role="form" data-parsley-validate="" >

                            {{--NAMA--}}
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Nama Pelapor</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" placeholder="Nama" name="name" data-parsley-trigger="change"
                                           required data-parsley-minlength="4">
                                    {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                                </div>
                            </div>

                            {{--CABANG--}}
                            <div class="form-group">
                                <label for="branch" class="col-sm-2 control-label">Tipe Kantor</label>
                                <div class="col-sm-10">
                                    <select name="branch" id="branch" data-parsley-trigger="change" required class="form-control mb-10">
                                        <option value="">Pilih Unit Kerja...</option>
                                        @foreach( $branch as $value)
                                            <option value="{{ $value->id }}">{{ $value->branch_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            {{--UNIT KERJA--}}
                            <div class="form-group">
                                <label for="unit" class="col-sm-2 control-label">Nama Unit Kerja</label>
                                <div class="col-sm-8">
                                    <select name="unit" id="unit" data-parsley-trigger="change" required class="form-control mb-10">
                                        <option>Pilih Nama Unit Kerja...</option>
                                    </select>
                                </div>
                            </div>

                            {{--JABATAN--}}
                            <div class="form-group">
                                <label for="position" class="col-sm-2 control-label">Jabatan Pelapor</label>
                                <div class="col-sm-8">
                                    <select name="position" id="position" data-parsley-trigger="change" required class="form-control mb-10">
                                        <option>Pilih Jabatan...</option>
                                    </select>
                                </div>
                            </div>

                            {{--ALAMAT--}}
                            <div class="form-group">
                                <label for="address" class="col-sm-2 control-label">Alamat Pelapor</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="address" data-parsley-trigger="change" required> </textarea>
                                    <p class="help-block mb-0">Isikan Alamat Pelapor</p>
                                </div>
                            </div>

                            {{--EMAIL--}}
                            <div class="form-group">
                                <label for="email" class="col-sm-2 control-label">Email Pelapor</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" placeholder="Email" name="email" data-parsley-trigger="change" required>
                                    {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                                </div>
                            </div>

                            {{--TELPHONE--}}
                            <div class="form-group">
                                <label for="tlp" class="col-sm-2 control-label">Telpon Pelapor</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="tlp" placeholder="No Telpon" name="tlp" data-parsley-trigger="change" required>
                                    {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                                </div>
                            </div>

                            <h3 class="custom-font">Data Kejadian</h3>
                            <hr/>

                            {{--PIHAK TERLIBAT--}}
                            <div class="form-group">
                                <label for="involved_person" class="col-sm-2 control-label">Pihak Terlibat</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="involved_person" data-parsley-trigger="change" required> </textarea>
                                    <p class="help-block mb-0">Isikan pihak yang terlibat.</p>
                                </div>
                            </div>

                            {{--BENTUK PELANGGARAN--}}
                            <div class="form-group">
                                <label for="action_type" class="col-sm-2 control-label">Jenis / Bentuk Pelanggaran</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="action_type" data-parsley-trigger="change" required> </textarea>
                                    <p class="help-block mb-0">Isikan jenis pelanggaran.</p>
                                </div>
                            </div>

                            {{--WAKTU KEJADIAN--}}
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Waktu Kejadian</label>
                                <div class="col-sm-10">
                                    <div class='input-group '>
                                        <input id="incident_time" type='text' class="form-control" name="incident_time" data-parsley-trigger="change" required/>
                                        <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                    </div>
                                </div>
                            </div>

                            {{--LOKASI KEJADIAN--}}
                            <div class="form-group">
                                <label for="incident_loc" class="col-sm-2 control-label">Lokasi Kejadian</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="incident_loc" data-parsley-trigger="change" required> </textarea>
                                    <p class="help-block mb-0">Isikan lokasi kejadian.</p>
                                </div>
                            </div>

                            {{--INDIKASI / MOTIF--}}
                            <div class="form-group">
                                <label for="indication" class="col-sm-2 control-label">Indikasi / Motif</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="indication" data-parsley-trigger="change" required> </textarea>
                                    <p class="help-block mb-0">Deskripsikan indikasi atau motif.</p>
                                </div>
                            </div>

                            {{--KRONOLOGIS--}}
                            <div class="form-group">
                                <label for="chronology" class="col-sm-2 control-label">Kronologis</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="chronology" data-parsley-trigger="change" required> </textarea>
                                    <p class="help-block mb-0">Deskripsikan kronologis kejadian.</p>
                                </div>
                            </div>

                            {{--INDIKASI KERUGIAN--}}
                            <div class="form-group">
                                <label for="loss" class="col-sm-2 control-label">Indikasi Kerugian</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="loss" data-parsley-trigger="change" required> </textarea>
                                    <p class="help-block mb-0">Isikan indikasi kerugian dapat berupa kerugian finansial atau kerugian non finansial.</p>
                                </div>
                            </div>

                            {{--DOKUMEN PENDUKUNG--}}
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Dokumen Pendukung</label>
                                <div class="col-sm-10">
                                    <input type="file" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox" name="file" data-parsley-trigger="change" required>
                                    <p class="help-block mb-0">Dokumen yang dapat di upload maksimal 1(satu). Mengingat keterbatasan bandwith dari system kami, maka dokumen pendukung lainnya dapat saudara kirimkan saat berkoordinasi langsung dengan admin.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a id="send" class="btn btn-rounded btn-primary btn-sm" onclick="sendReport()">Send</a>
                                    {{--<button id="send" type="submit" class="btn btn-rounded btn-primary btn-sm">Kirim</button>--}}
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>

            <div class="col-md-6">
                <section class="tile">
                    <div class="tile-header dvd dvd-btm">
                        <h1 class="custom-font">Cek Status <strong>Laporan Pengaduan</strong> Anda !</h1>
                    </div>
                    <div class="tile-body">
                        <form class="form-horizontal finput" role="form" data-parsley-validate="">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">No Ticket</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="No Ticket" name="no_ticket" id="no_ticket">
                                    <p class="help-block mb-0">Masukan no ticket anda yang diperoleh dari email notifikasi.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a id="cek" class="btn btn-rounded btn-primary btn-sm" onclick="cekTicket()">Cek</a>
                                    {{--<button id="cek" type="submit" class="btn btn-rounded btn-primary btn-sm">Cek</button>--}}
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $(function () {
                $('#incident_time').datetimepicker({
                    format: 'YYYY-MM-DD H:m:ss'
                });
            });
            $('#branch').on('change', function () {
                var branch_id = $(this).val();
                if(branch_id){

                    $.get("{{url('/frontend/unit')}}/"+branch_id, function (data) {
                        $('#unit').empty();
                        $.each(data, function (i,item) {
                            $('#unit').append($('<option>',{
                                value: data[i].id,
                                text: data[i].unit_name
                            }));
                        })
                    },'json');

                }else{
                    $('#unit').empty();
                }
            });

            $('#unit').on('change', function () {
                var unit_id = $(this).val();

                if(unit_id){

                    $.get("{{url('/frontend/position')}}/"+unit_id, function (data) {
                        $('#position').empty();
                        $.each(data, function (i,item) {
                            $('#position').append($('<option>',{
                                value: data[i].id,
                                text: data[i].position_name
                            }));
                        })
                    },'json');
                }else{
                    $('#position').empty();
                }
            });
        });


        var form  = $('#report');
        var modal = $('#myModal');
        var loader = $('.ui.dimmable #main_loader');

        function cekTicket() {
            var no_ticket = $('#no_ticket').val();
            console.log(no_ticket)
            $.ajax({
                type: 'GET',
                url: 'frontend/ticket/'+no_ticket,
                processData: false,
                contentType: false,
                success: function (res) {
                    var data = $.parseJSON(res)
                    if(data.status == 0){
                        //toastr.error("Kode tiket tidak ditemukan!")
                        //modal.hide('hide');

                        notfound('Kode tiket tidak ditemukan!')
                    }

                    if(data.status == 1){

                        var status_ticket = data.data[0].status;

                        switch (status_ticket){
                            case 0:
                                $('#m_status').addClass('bg-default');
                                $('#m_status').html('Laporan Diterima');
                                break;
                            case 1:
                                $('#m_status').addClass('bg-cyan');
                                $('#m_status').html('Laporan Diproses');
                                break;
                            case 2:
                                $('#m_status').addClass('bg-green');
                                $('#m_status').html('Investigasi');
                                break;
                            case 3:
                                $('#m_status').addClass('bg-danger');
                                $('#m_status').html('Laporan Ditolak!');
                                break;
                        }

                        $('#m_noket').html(no_ticket);
                        modal.modal("show");
                    }

                }});
        }

        function notfound(pesan) {
            toastr.error(pesan);
        }
        function closeModal() {
            modal.hide('hide');
            $('#m_status').removeClass();
        }
        function sendReport() {
            form.parsley().validate();
            if(form.parsley().isValid()){
                var dataReport = new FormData(form[0]);
                loader.dimmer({closable: false}).dimmer('show');
                add('frontend/save', dataReport);
                form[0].reset();
            }
        }

        function add(url, formdata) {
            $.ajax({
                type: 'POST',
                url: url,
                data: formdata,
                processData: false,
                contentType: false,
                success: function (res) {
                    loader.dimmer({closable: false}).dimmer('hide');
                    toastr.success("Input Data Sukses!");
                }
            });
        }
    </script>
@endsection