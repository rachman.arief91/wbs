<div class="modal" id="m_add_unit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font"><strong>Tambah Unit Kerja</strong></h3>
            </div>
            <div class="modal-body">
                @include('unit.create')
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" id="m_btn_add_unit" onclick="processInsert()"><i class="fa fa-save"></i> Simpan</button>
                <button class="btn btn-danger btn-ef btn-ef-3 btn-ef-3c" data-dismiss="modal" onclick="closeModal()"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>