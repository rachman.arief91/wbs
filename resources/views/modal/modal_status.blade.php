<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font"><strong>INFO</strong></h3>
            </div>
            <div class="modal-body">
                Status laporan dengan nomor tiket <strong><span id="m_noket"></span></strong> : <mark id="m_status" class="bg-">Default Label</mark>
            </div>
            <div class="modal-footer">
                {{--<button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c"><i class="fa fa-arrow-right"></i> Submit</button>--}}
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" onclick="closeModal()"><i class="fa fa-arrow-left"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>