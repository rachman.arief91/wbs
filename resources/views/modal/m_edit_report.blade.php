@php( $role = Session::get('user')->role_id)
<div class="modal" id="m_edit_report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font"><strong>Tindak Lanjut</strong></h3>
            </div>
            <div class="modal-body">
                <section class="tile">
                    <h4 class="custom-font">Deskripsi Laporan</h4>
                    <dl class="dl-horizontal filled bg-dutch dk">
                        <dt>Nama Pelapor</dt>
                        <dd class="bg-dutch" id="d_name"></dd>
                        <dt>Email</dt>
                        <dd class="bg-dutch" id="d_email"></dd>
                        <dt>Telpon</dt>
                        <dd class="bg-dutch" id="d_telp"></dd>
                        <dt>No Tiket</dt>
                        <dd class="bg-dutch" id="d_ticket"></dd>
                        <dt>Tanggal Laporan</dt>
                        <dd class="bg-dutch" id="d_date"></dd>
                    </dl>
                </section>
                <form id="report_edit" enctype="multipart/form-data" name="report" class="form-horizontal" role="form"
                      data-parsley-validate="">
                    @if($role == 0)
                        <div class="form-group">
                            <label for="loss" class="col-sm-2 control-label">Analisis Awal</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" name="prem_study" id="prem_study" required
                                          data-parsley-trigger="change"> </textarea>
                            </div>
                        </div>
                    @endif

                    @if($role == 1)
                        <div class="form-group">
                            <label for="loss" class="col-sm-2 control-label">Analisis Awal</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" name="prem_study" id="prem_study" required
                                          data-parsley-trigger="change" readonly> </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="loss" class="col-sm-2 control-label">Usulan PinGroup</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" name="pingroup_prop" id="pingroup_prop" required
                                          data-parsley-trigger="change"> </textarea>
                            </div>
                        </div>
                    @endif
                    @if($role == 2)
                        <div class="form-group">
                            <label for="loss" class="col-sm-2 control-label">Analisis Awal</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" name="prem_study" id="prem_study" required
                                          data-parsley-trigger="change" readonly> </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="loss" class="col-sm-2 control-label">Usulan PinGroup</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" name="pingroup_prop" id="pingroup_prop" required
                                          data-parsley-trigger="change" readonly> </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="loss" class="col-sm-2 control-label">Keputusan PinDiv</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="5" name="pindiv_dec" id="pindiv_dec" required
                                          data-parsley-trigger="change"> </textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="branch" class="col-sm-2 control-label">Keputusan</label>
                            <div class="col-sm-10">
                                <select name="status" id="decision_id" data-parsley-trigger="change" required
                                        class="form-control mb-10">
                                    <option value="">Pilih Keputusan</option>
                                    <option value="3">Audit Investigasi</option>
                                    <option value="4">Di Tolak</option>
                                </select>
                            </div>
                        </div>
                    @endif
                    <input type="hidden" id="report_id" name="id"/>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" id="m_btn_add_branch"
                        onclick="updateStatus()"><i class="fa fa-save"></i> Simpan
                </button>
                <button class="btn btn-danger btn-ef btn-ef-3 btn-ef-3c" data-dismiss="modal" onclick="closeModal()"><i
                            class="fa fa-close"></i> Tutup
                </button>
            </div>
        </div>
    </div>
</div>