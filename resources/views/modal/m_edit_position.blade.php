<div class="modal" id="m_edit_position" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title custom-font"><strong>Edit Jabatan</strong></h3>
            </div>
            <div class="modal-body">
                @include('position.edit')
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" id="m_btn_add_position" onclick="processUpdate()"><i class="fa fa-save"></i> Simpan</button>
                <button class="btn btn-danger btn-ef btn-ef-3 btn-ef-3c" data-dismiss="modal" onclick="closeEditModal()"><i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>