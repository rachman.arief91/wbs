@extends('admin.main')

@section('content')


    <section class="tile">
        <div class="tile-header dvd dvd-btm">
            <h1 class="custom-font"><strong>Unit Kerja</strong></h1>
        </div>
        <div class="tile-footer text-left bg-tr-black lter dvd dvd-top mb-10">
            <div class="ml-20">
                <div id="tableTools">
                    <button type="button" class="btn btn-primary btn-sm mb-10" id="i_btn_add" onclick="showAdd()"><i class="fa fa-plus"></i> Tambah Unit Kerja</button>
                </div>
            </div>
        </div>
        <div class="tile-body ml-20">

            <div class="table">
                <form id="form" role="form">
                    {{csrf_field()}}
                    <input type="hidden" name="search" id="search">
                    <input type="hidden" name="order" id="order">
                </form>
                <table class="ui striped padded celled table standard-usage" id="tbl_unit" width="100%">
                    <thead class="full-width single line">
                    <tr>
                        <th style="width: 5%;" class="no-sort collapsing center aligned">No</th>
                        <th style="width: 40%;" class="no-sort">Unit Kerja</th>
                        <th style="width: 30%;" class="no-sort">Cabang</th>
                        <th style="width: 20%;" class="single line">Deskripsi</th>
                        <th style="width: 5%;" class="no-sort single line">Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </section>


    {{-- </div>
 </div>
--}}

@endsection

@include('modal.m_add_unit')
@include('modal.m_edit_unit')

@section('tableunit')
    <script type="text/javascript">

        var modal = $('#m_add_unit');
        var modal_upd = $('#m_edit_unit');

        var unit_tbl = $('#tbl_unit').DataTable({
            serverSide: true,
            pagingType: 'full_numbers',
            ajax: {
                url: 'api/unit',
                type: 'GET'
            },
            columns: [
                {data: 'id', name: 'm_unit.id' },
                {data: 'unit_name', name: 'm_unit.unit_name'},
                {data: 'branch_name', name: 'm_branch.branch_name'},
                {data: 'note', name: 'm_unit.note'},
                {data: 'action', name: 'action', searchable: false, orderable: false}
            ]
        });

        function processInsert () {
            var form = $('#m_form_add_unit');
            form.parsley().validate();
            if(form.parsley().isValid()){
                var dataReport = new FormData(form[0]);
                add('api/unit/store', dataReport);
                form[0].reset();
                modal.hide('hide');
            }

        }

        function showAdd() {
            modal.show('show');
        }

        function closeModal() {
            modal.hide('hide');
        }

        function closeEditModal() {
            modal_upd.hide('hide');
        }


        function showEdit(id) {
            $.ajax({
                type: 'GET',
                url: 'unit/'+id,
                success: function (res) {
                    var data = $.parseJSON(res);
                    $('#e_unit_name').val(data[0].unit_name);
                    $('#e_unit_note').val(data[0].note);
                    $('#e_unit_branch').val(data[0].branch_id);

                }
            });
            console.log(id);
            $('#e_unit_id').val(id);

            modal_upd.show('show');
        }

        function processEdit() {
            var form = $('#m_form_edit_unit');
            form.parsley().validate();
            if(form.parsley().isValid()){
                var dataUpdate = new FormData(form[0]);
                edit('unit/update', dataUpdate);
                modal_upd.hide('hide');
            }
        }

        function edit(url, formdata) {
            $.ajax({
                type: 'POST',
                url: url,
                data: formdata,
                processData: false,
                contentType: false,
                success: function (res) {
                    var data = $.parseJSON(res)
                    if (data.status == 1) {
                        toastr.success("Update Data Sukses!");
                    }
                    if (data.status == 0) {
                        toastr.error("Terjadi Kesalahan");
                    }
                    unit_tbl.ajax.reload();
                }
            });
        }

        function add(url, formdata) {
            $.ajax({
                type: 'POST',
                url: url,
                data: formdata,
                processData: false,
                contentType: false,
                success: function (res) {
                    var data = $.parseJSON(res)
                    if(data.status == 1){
                        toastr.success("Input Data Sukses!");
                    }
                    if(data.status == 0){
                        toastr.error("Terjadi Kesalahan");
                    }

                    unit_tbl.ajax.reload();
                }
            });
        }

    </script>
@endsection