<div class="row">
    <div class="col-md-12">
        <section class="tile">
            <div class="tile-body">
                <form method='post' action="{{--{{ route('refcabang.store') }}--}}" class="form-horizontal" id="m_form_add_unit" data-parsley-validate="">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Nama Unit </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="unit_name" name="unit_name" data-parsley-trigger="change"
                                   required data-parsley-minlength="4">
                            {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="note" name="note" data-parsley-trigger="change"
                                   required data-parsley-minlength="4">
                            {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="branch" class="col-sm-2 control-label">Cabang</label>
                        <div class="col-sm-10">
                            <select name="branch_id" id="branch" data-parsley-trigger="change" required class="form-control mb-10">
                                <option value="">Pilih Cabang...</option>
                                @foreach( $branch as $value)
                                    <option value="{{ $value->id }}">{{ $value->branch_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>