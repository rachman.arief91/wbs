<!doctype html>
<html lang="{{app()->getLocale()}}">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>WBS - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('css/vendor/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ui/loader.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ui/dimmer.min.css') }}">
</head>

<body id="minovate" class="appWrapper">
    <div id="wrap" class="animsition">
        <div class="page page-core page-login " style="background: url(images/bg.jpg);">
            <div class="container w-420 p-20 bg-white mt-40 text-center  ui dimmable">
                <div class="ui inverted dimmer" id="main_loader">
                    <div class="ui loader"></div>
                </div>
                <h3 class="text-light" style="color:#191815;font-weight: normal!important;">Whisle Blowing System</h3>
                <form method="POST" action="{{ route('login') }}" name="formLogin" role="form" id="formLogin" class="form-validation mt-20">
                    {{ csrf_field() }}
                    <p id="error-msg" class="help-block text-danger text-left"/>
                    <div class="form-group">
                            <input id="username" type="text" name="username" value="{{ old('username') }}" class="form-control underline-input"
                                   data-parsley-trigger="change"
                                   data-parsley-length="[4, 20]"
                                   required >
                    </div>
                    <div class="form-group">
                            <input id="password" type="password" class="form-control underline-input" name="password" data-parsley-trigger="change" required>
                    </div>
                    <div class="form-group text-right mt-20">
                        <input type="submit" id="login-btn" value="Login" style="background-color: #911515;" class="btn btn-darkgray text-uppercase b-0 br-2 mr-6">
                        </div>
                </form>
            </div>
        </div>
    </div>
</body>
