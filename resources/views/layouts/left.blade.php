<aside id="sidebar">
    <div id="sidebar-wrap">
        <div class="panel-group slim-scroll" role="tablist">
            <div class="panel panel-default">
                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">
                        <ul id="navigation">
                            <li class=""><a href="{!! url('/frontend') !!}"><i class="fa fa-home"></i><span>Home</span></a></li>
                            <li class=""><a href="{!! url('/panduan') !!}"><i class="fa fa-book"></i><span>Panduan</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>