<section id="header">
    <header class="clearfix">
        <div class="branding">
            <a class="brand" href="/bcms-bjbs/">
                <span>Whisle Blowing System {{--<strong>BJBS</strong>--}}</span>
            </a>
            <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
        </div>
        {{--<ul class="nav-left pull-left list-unstyled list-inline">
            <li class="sidebar-collapse">
                <div class="ui green label center">
                    JABATAB
                </div><div class="ui yellow label">
                    UNIT
                </div><a role="button" tabindex="0" class="collapse-sidebar" style="pointer-events: none;cursor: default;color: #fff;">
                    <strong>Role</strong>
                </a>
            </li>
        </ul>--}}
        <ul class="nav-right pull-right list-inline">
            <li class="dropdown nav-profile">
                <a href class="dropdown-toggle" data-toggle="dropdown">
                    <span>{{Session::get('name')}} <i class="fa fa-angle-down"></i></span>
                </a>
                <ul class="dropdown-menu pull-right panel panel-default animated littleFadeInUp" role="menu">
                    <li><a href="{{ route('logout')}}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Logout</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </ul>
            </li>
        </ul>
    </header>
</section>