<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.head')
</head>
<body id="minovate" class="appWrapper hz-menu">
<div id="wrap" class="animsition" style="height:100%;">
    @include('layouts.header')
    <div id="controls">
        @include('layouts.left')
    </div>
    <section>
        <div id="content" class="ui dimmable">
            <div class="ui dimmer" id="main_loader">
                <div class="ui loader"></div>
            </div>
            <div id="data_content">
                @yield('content')
            </div>
        </div>
    </section>
</div>
<div id="data_modal">@include('modal.modal_status')</div>
@include('layouts.script')
</body>
</html>
