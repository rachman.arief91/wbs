<div class="row">
    <div class="col-md-12">
        <section class="tile">
            <div class="tile-body">
                <form method='post' action="{{--{{ route('refcabang.store') }}--}}" class="form-horizontal" id="m_form_add_branch" data-parsley-validate="">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Nama Cabang </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="Nama" name="branch_name" data-parsley-trigger="change"
                                   required data-parsley-minlength="4">
                            {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" placeholder="Nama" name="note" data-parsley-trigger="change"
                                   required data-parsley-minlength="4">
                            {{--<p class="help-block mb-0">Example block-level help text here.</p>--}}
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>